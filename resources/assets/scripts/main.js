// import external dependencies
import 'jquery';

// Import everything from autoload
import './autoload/**/*';

// import local dependencies
import Router from './util/Router';
import common from './routes/common';
import home from './routes/home';
import templateAbout from './routes/about';
import templatePortfolio from './routes/templatePortfolio';
import templateLocations from './routes/templateLocations';


import templateCareersCw from './routes/careers-cw';
import templateCareersDm from './routes/careers-dm';
import templateCareersDev from './routes/careers-dev';
// import then needed Font Awesome functionality
import { library, dom } from '@fortawesome/fontawesome-svg-core';
// import the Facebook and Twitter icons
import {
  faFacebookF,
  faInstagram,
  faLinkedinIn,
  faBehance,
} from '@fortawesome/free-brands-svg-icons';

library.add(faFacebookF, faInstagram, faLinkedinIn, faBehance);

// tell FontAwesome to watch the DOM and add the SVGs when it detects icon markup
dom.watch();

/** Populate Router instance with DOM routes */
const routes = new Router({
  // All pages
  common,
  // Home page
  home,
  // About Us page, note the change from about-us to aboutUs.
  templateAbout,

  templatePortfolio,

  templateCareersCw,

  templateCareersDm,

  templateCareersDev,

  templateLocations,
});

// Load Events
jQuery(document).ready(() => routes.loadEvents()); 