export default {
  init() {
    // JavaScript to be fired on all pages
    let menu_bars = document.getElementById('menu-bars');
    let menu_display = document.getElementsByClassName('burger-menu-content')[0];
    let menu_links = document.getElementById('nav-phone').getElementsByClassName('menu-item');
    let html = $('html');

    menu_bars.addEventListener('click', function () {
      if (menu_bars.dataset.clicked == 0) {
        this.classList.add('burger-x');
        this.classList.remove('burger-regular');
        menu_display.style.transform = 'scale(1, 1)';
        menu_display.style.height = '100vh';
        menu_display.style.position = 'fixed';
        menu_display.style.width = '100%';
        html.css('overflow', 'hidden');
        for (let i = 0; i < menu_links.length; i++) {
          menu_links[i].style.opacity = '1'
        }
        menu_bars.dataset.clicked = 1;
      } else if (menu_bars.dataset.clicked == 1) {
        this.classList.add('burger-regular');
        this.classList.remove('burger-x');
        menu_bars.dataset.clicked = 0;
        menu_display.style.transform = 'scale(1, 0)';
        html.css('overflow', 'auto');
        for (let i = 0; i < menu_links.length; i++) {
          menu_links[i].style.opacity = '0'
        }
      }
    })

    try {
      $('.hidden-service').click(function() {
        $(this).hide();
        $(this).parent().next().slideDown();
      });
    } catch(ex) {
      throw ex;
    }
    

    let posts = document.getElementsByClassName('post-carousel');

    if (posts.length > 0) {
      posts[0].classList.add('active');
    }

  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired

    /* eslint-disable */
    let projectForm = $('#project-form');

    projectForm.on('submit', (e) => {
      e.preventDefault();

      let project_type = document.querySelector('#project-form #mySelect').value;
      let business_name = document.querySelector('#project-form #business-name').value;
      let client_name = document.querySelector('#project-form #client-name').value;
      // let phone_num = document.querySelector('#project-form #client-phone-num').value;
      let email_addr = document.querySelector('#project-form #client-email-addr').value;
      let hutk = document.cookie.split('; ')
                                .find(row => row.startsWith('hubspotutk='))
                                .split('=')[1]
                                .toString();

      let sendInfo = {
        "fields": [
          {
            "name": "email",
            "value": email_addr
          },
          {
            "name": "firstname",
            "value": client_name
          },
          {
            "name": "project_name",
            "value": business_name
          },
          {
            "name": "project_type",
            "value": project_type
          },
          // {
          //   "name": "phone",
          //   "value": phone_num
          // }
        ],
        "context": {
          "pageUri": window.location.toString(),
          "hutk": hutk,
          "pageName": location.href.split("/").slice(-1).toString()
        }
      }

      $.ajax({
        url: "https://api.hsforms.com/submissions/v3/integration/submit/4773320/7add9395-082e-4e41-883b-ee6d6535f682",
        method: "POST",
        dataType: "json",
        data: JSON.stringify(sendInfo),
        contentType: "application/json",

        success: () => {
          window.location.replace("https://alphaefficiency.com/thank-you-page");
        },
        err: (err, xhr) => {
          console.log(err, xhr);
        }
      })
    });
  },
};
