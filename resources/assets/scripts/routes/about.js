export default {
  init() {
    // JavaScript to be fired on the about us page
    $('.see-more').click(function(e){
      e.preventDefault();
      $('.first').attr('style', 'display: block !important;');
      $(this).attr('style', 'display: none !important;');
      $('.productivity').css('height', '1670px');
      $('.hide').slideDown();
    });
  },
};
