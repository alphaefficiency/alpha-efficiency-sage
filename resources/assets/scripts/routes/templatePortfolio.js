export default {
  init() {
    $('section .container .img-wrap').on('click', (e) => {
      e.stopImmediatePropagation();

      let width = window.innerWidth;

      if(width <= 600) {
        let text = $(e.target).parent().next().find('.inner');
        text.slideToggle()
      }
    });

    $('.detail-up-btn').on('click', (e) => {
      e.preventDefault();
      e.stopImmediatePropagation();

      let width = window.innerWidth;

      if(width <= 600) {
        let text = $(e.target).parent().parent().parent().parent().find('.inner');
        text.slideToggle()
      }
    });
  },
  finalize() {


  },
};
