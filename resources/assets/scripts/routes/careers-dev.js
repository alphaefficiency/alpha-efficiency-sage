export default {
  init() {
    function myFunction() {
      var up = document.getElementById('up');
      var down = document.getElementById('down');
      var ul = document.getElementById('ul');
      if (ul.style.display === 'none') {
        ul.style.display = 'block';
        down.style.display = 'none';
        up.style.display = 'inline';
      } else {
        ul.style.display = 'none';
        up.style.display = 'none';
        down.style.display = 'inline';
      }
    }

    let up = $('#up');
    let down = $('#down');
    up.click(myFunction);
    down.click(myFunction);

    function thisFunction() {
      var up2 = document.getElementById('up2');
      var down2 = document.getElementById('down2');
      var ul2 = document.getElementById('ul2');
      if (ul2.style.display === 'none') {
        ul2.style.display = 'block';
        down2.style.display = 'none';
        up2.style.display = 'inline';
      } else {
        ul2.style.display = 'none';
        up2.style.display = 'none';
        down2.style.display = 'inline';
      }
    }
    let up2 = $('#up2');
    let down2 = $('#down2');
    up2.click(thisFunction);
    down2.click(thisFunction);
  },
  finalize() {


  },
};