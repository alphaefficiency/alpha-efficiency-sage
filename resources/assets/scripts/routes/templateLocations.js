export default {
  init() {
    // JavaScript to be fired on the home page
  },
  finalize() {
     // JavaScript to be fired on the locations page

    /* eslint-disable */

    $(function() {
      $(window).on("scroll", function() {
          if($(window).scrollTop() > 50) {
              $("header").addClass("active");
          } else {
              //remove the background property so it comes transparent again (defined in your css)
             $("header").removeClass("active");
          }
      });
  });
  },
};