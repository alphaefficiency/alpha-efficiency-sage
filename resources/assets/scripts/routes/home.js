export default {
  init() {
    // JavaScript to be fired on the home page
    $(document).ready(function () {
      const deviceWidth = $(document).width();
      const logo_img_desk = $('.logo-img-desk');
      const logo_img_phone = $('.logo-img-phone');
      const logo_scroll = $('.logo-img-scroll');

      window.onload = () => {
        if (deviceWidth > 480) {
          logo_img_desk.removeClass('d-none');
          logo_scroll.addClass('d-none');
        } else if (deviceWidth <= 480) {
          logo_img_desk.hide();
          logo_img_phone.removeClass('d-none');
        }
      }

      //setup scroll action
      window.onscroll = () => {
        const logo_img_desk = $('.logo-img-desk');
        
        const logo_scroll = $('.logo-img-scroll');

        const menu_holder = $('.menu-holder');
        const home_dots = $('#home-dots');

        let status = false;

        const scrollHeight = window.pageYOffset;
        const deviceWidth = $(document).width();
        if (scrollHeight > 50) {
          logo_img_desk.addClass('d-none');

          home_dots.css({ 'margin-right': '17.5%' });

          if (deviceWidth <= 1710) {
            home_dots.css({ 'margin-right': '18.8%' });
          }

          if (deviceWidth <= 1500) {
            home_dots.css({ 'margin-right': '22.15%' });
          }

          if (deviceWidth <= 1366) {
            home_dots.css({ 'margin-right': '23.5%' });
          }

          if (deviceWidth <= 1280) {
            home_dots.css({ 'margin-right': '24.8%' });
          }

          status = true;

          if (status) {
            logo_scroll.removeClass('d-none');

            if(deviceWidth <= 900) {
              logo_scroll.addClass('d-none');
            }
          }
        }
        else if (scrollHeight < 50) {
          menu_holder.css({ 'width': '90%' })

          logo_scroll.addClass('d-none');
          status = true;

          if (status) {
            logo_img_desk.removeClass('d-none')
            home_dots.css({ 'margin-right': '0' });
          }
        }
      }

      const sections = ['make', 'grow', 'intelligence', 'clients', 'explore', 'portfolio', 'contact'];
      let dot_holder = $('#home-dots');
      let content = '';
      let dot = $('.dot-holder');
      if (window.innerWidth <= 480) {
        dot.eq(0).children().css({ 'width': '18', 'height': 18 })

      } else {
        dot.eq(0).children().css({ 'width': '20', 'height': 20 })
      }

      for (let i = 0; i < sections.length; i++) {
        content += `
          <div class="dot">
            <a class="dot-holder" href="#${sections[i]}"><svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
            <circle cx="7.5" cy="7.49994" r="7.5" transform="rotate(-180 7.5 7.49994)" fill="#E2E2E2"/>
            </svg>
            </a>
          </div>
          `;
      }

      function debounce(func, wait = 10, immediate = true) {
        var timeout;
        return function () {
          var context = this, args = arguments;

          var later = function () {
            timeout = null;
            if (!immediate) func.apply(context, args);
          }

          var callNow = immediate && !timeout;
          clearTimeout(timeout);
          timeout = setTimeout(later, wait);
          if (callNow) func.apply(context, args);
        }
      }

      let menu_bars = document.getElementById('menu-bars-scroll');
      let menu_display = document.getElementsByClassName('burger-menu-content')[0];
      let menu_links = document.getElementById('nav-phone').getElementsByClassName('menu-item');
      let html = $('html');

      menu_bars.addEventListener('click', function () {
        if (menu_bars.dataset.clicked == 0) {
          this.classList.add('burger-x');
          this.classList.remove('burger-regular');
          menu_display.style.transform = 'scale(1, 1)';
          menu_display.style.height = '100vh';
          menu_display.style.position = 'fixed';
          menu_display.style.width = '100%';
          html.css('overflow', 'hidden');
          for (let i = 0; i < menu_links.length; i++) {
            menu_links[i].style.opacity = '1'
          }
          menu_bars.dataset.clicked = 1;
        } else if (menu_bars.dataset.clicked == 1) {
          this.classList.add('burger-regular');
          this.classList.remove('burger-x');
          menu_bars.dataset.clicked = 0;
          menu_display.style.transform = 'scale(1, 0)';
          html.css('overflow', 'auto');
          for (let i = 0; i < menu_links.length; i++) {
            menu_links[i].style.opacity = '0'
          }
        }
      })

      function checkSlide() {
        let dot = $('.dot-holder');
        let header = $('#dots-menu');
        //For each slider image do
        if (window.innerWidth > 900) {
          // if (window.scrollY >= 0) {
          //   header.css({ 'display': 'flex', 'flex-direction': 'row-reverse', 'align-items': 'center' });
          //   dot.children().children().css({ 'width': '20', 'height': 20, 'fill': '#E2E2E2' })
          // } else if (window.scrollY < 750) {
          //   header.fadeOut();
          // }

          if (window.scrollY <= 750) {
            dot.eq(0).children().css({ 'width': '20', 'height': 20 })
            dot.eq(0).children().children().css({ 'width': '20', 'height': 20, 'fill': '#CFCFCF' })
            dot.eq(1).children().css({ 'width': '15', 'height': 15 })
            dot.eq(2).children().css({ 'width': '15', 'height': 15 })
            dot.eq(3).children().css({ 'width': '15', 'height': 15 })
            dot.eq(4).children().css({ 'width': '15', 'height': 15 })
            dot.eq(5).children().css({ 'width': '15', 'height': 15 })
            dot.eq(6).children().css({ 'width': '15', 'height': 15 })
          }
          else if (window.scrollY >= 750 && window.scrollY < 1666) {
            dot.eq(0).children().css({ 'width': '15', 'height': 15 })
            dot.eq(1).children().css({ 'width': '20', 'height': 20 })
            dot.eq(1).children().children().css({ 'width': '20', 'height': 20, 'fill': '#CFCFCF' })
            dot.eq(2).children().css({ 'width': '15', 'height': 15 })
            dot.eq(3).children().css({ 'width': '15', 'height': 15 })
            dot.eq(4).children().css({ 'width': '15', 'height': 15 })
            dot.eq(5).children().css({ 'width': '15', 'height': 15 })
            dot.eq(6).children().css({ 'width': '15', 'height': 15 })
          }
          else if (window.scrollY >= 1667 && window.scrollY < 2499) {
            dot.eq(0).children().css({ 'width': '15', 'height': 15 })
            dot.eq(2).children().css({ 'width': '20', 'height': 20 })
            dot.eq(2).children().children().css({ 'width': '20', 'height': 20, 'fill': '#CFCFCF' })
            dot.eq(6).children().css({ 'width': '15', 'height': 15 })
            dot.eq(5).children().css({ 'width': '15', 'height': 15 })
            dot.eq(4).children().css({ 'width': '15', 'height': 15 })
            dot.eq(3).children().css({ 'width': '15', 'height': 15 })
            dot.eq(1).children().css({ 'width': '15', 'height': 15 })
          }
          else if (window.scrollY >= 2500 && window.scrollY < 3332) {
            dot.eq(0).children().css({ 'width': '15', 'height': 15 })
            dot.eq(3).children().css({ 'width': '20', 'height': 20 })
            dot.eq(3).children().children().css({ 'width': '20', 'height': 20, 'fill': '#CFCFCF' })
            dot.eq(6).children().css({ 'width': '15', 'height': 15 })
            dot.eq(5).children().css({ 'width': '15', 'height': 15 })
            dot.eq(4).children().css({ 'width': '15', 'height': 15 })
            dot.eq(2).children().css({ 'width': '15', 'height': 15 })
            dot.eq(1).children().css({ 'width': '15', 'height': 15 })
          }
          else if (window.scrollY >= 3333 && window.scrollY < 4214) {
            dot.eq(0).children().css({ 'width': '15', 'height': 15 })
            dot.eq(4).children().css({ 'width': '20', 'height': 20 })
            dot.eq(4).children().children().css({ 'width': '20', 'height': 20, 'fill': '#CFCFCF' })
            dot.eq(6).children().css({ 'width': '15', 'height': 15 })
            dot.eq(5).children().css({ 'width': '15', 'height': 15 })
            dot.eq(3).children().css({ 'width': '15', 'height': 15 })
            dot.eq(2).children().css({ 'width': '15', 'height': 15 })
            dot.eq(1).children().css({ 'width': '15', 'height': 15 })
          }
          else if (window.scrollY >= 4215 && window.scrollY < 5030) {
            dot.eq(0).children().css({ 'width': '15', 'height': 15 })
            dot.eq(5).children().css({ 'width': '20', 'height': 20 })
            dot.eq(5).children().children().css({ 'width': '20', 'height': 20, 'fill': '#CFCFCF' })
            dot.eq(6).children().css({ 'width': '15', 'height': 15 })
            dot.eq(4).children().css({ 'width': '15', 'height': 15 })
            dot.eq(3).children().css({ 'width': '15', 'height': 15 })
            dot.eq(2).children().css({ 'width': '15', 'height': 15 })
            dot.eq(1).children().css({ 'width': '15', 'height': 15 })
          }
          else if (window.scrollY >= 5031) {
            dot.eq(0).children().css({ 'width': '15', 'height': 15 })
            dot.eq(6).children().css({ 'width': '20', 'height': 20 })
            dot.eq(6).children().children().css({ 'width': '20', 'height': 20, 'fill': '#CFCFCF' })
            dot.eq(5).children().css({ 'width': '15', 'height': 15 })
            dot.eq(4).children().css({ 'width': '15', 'height': 15 })
            dot.eq(3).children().css({ 'width': '15', 'height': 15 })
            dot.eq(2).children().css({ 'width': '15', 'height': 15 })
            dot.eq(1).children().css({ 'width': '15', 'height': 15 })
          }
        } else if (window.innerWidth <= 480) {
          if (window.scrollY >= 513) {
            // header.css({ 'display': 'flex', 'flex-direction': 'row-reverse', 'align-items': 'center' }).fadeIn();
            // console.log(window.scrollY)
          } else if (window.scrollY < 513) {
            header.fadeOut();
          }
          if (window.scrollY <= 750) {
            dot.eq(0).children().css({ 'width': '15', 'height': 15 })
            dot.eq(0).children().children().css({ 'width': '15', 'height': 15, 'fill': '#CFCFCF' })
            dot.eq(1).children().css({ 'width': '14', 'height': 14 })
            dot.eq(1).children().children().css({ 'width': '14', 'height': 14, 'fill': '#E2E2E2' })
            dot.eq(2).children().css({ 'width': '14', 'height': 14 })
            dot.eq(2).children().children().css({ 'width': '14', 'height': 14, 'fill': '#E2E2E2' })
            dot.eq(3).children().css({ 'width': '14', 'height': 14 })
            dot.eq(3).children().children().css({ 'width': '14', 'height': 14, 'fill': '#E2E2E2' })
            dot.eq(4).children().css({ 'width': '14', 'height': 14 })
            dot.eq(4).children().children().css({ 'width': '14', 'height': 14, 'fill': '#E2E2E2' })
            dot.eq(5).children().css({ 'width': '14', 'height': 14 })
            dot.eq(5).children().children().css({ 'width': '14', 'height': 14, 'fill': '#E2E2E2' })
            dot.eq(6).children().css({ 'width': '14', 'height': 14 })
            dot.eq(6).children().children().css({ 'width': '14', 'height': 14, 'fill': '#E2E2E2' })
          }
          else if (window.scrollY >= 750 && window.scrollY < 1666) {
            dot.eq(0).children().css({ 'width': '14', 'height': 14 })
            dot.eq(0).children().children().css({ 'width': '14', 'height': 14, 'fill': '#E2E2E2' })
            dot.eq(1).children().css({ 'width': '18', 'height': 18 })
            dot.eq(1).children().children().css({ 'width': '18', 'height': 18, 'fill': '#CFCFCF' })
            dot.eq(2).children().css({ 'width': '14', 'height': 14 })
            dot.eq(2).children().children().css({ 'width': '14', 'height': 14, 'fill': '#E2E2E2' })
            dot.eq(3).children().css({ 'width': '14', 'height': 14 })
            dot.eq(3).children().children().css({ 'width': '14', 'height': 14, 'fill': '#E2E2E2' })
            dot.eq(4).children().css({ 'width': '14', 'height': 14 })
            dot.eq(4).children().children().css({ 'width': '14', 'height': 14, 'fill': '#E2E2E2' })
            dot.eq(5).children().css({ 'width': '14', 'height': 14 })
            dot.eq(5).children().children().css({ 'width': '14', 'height': 14, 'fill': '#E2E2E2' })
            dot.eq(6).children().css({ 'width': '14', 'height': 14 })
            dot.eq(6).children().children().css({ 'width': '14', 'height': 14, 'fill': '#E2E2E2' })
          }
          else if (window.scrollY >= 1667 && window.scrollY < 2499) {
            dot.eq(0).children().css({ 'width': '14', 'height': 14 })
            dot.eq(0).children().children().css({ 'width': '14', 'height': 14, 'fill': '#E2E2E2' })
            dot.eq(2).children().css({ 'width': '18', 'height': 18 })
            dot.eq(2).children().children().css({ 'width': '18', 'height': 18, 'fill': '#CFCFCF' })
            dot.eq(6).children().css({ 'width': '14', 'height': 14 })
            dot.eq(6).children().children().css({ 'width': '14', 'height': 14, 'fill': '#E2E2E2' })
            dot.eq(5).children().css({ 'width': '14', 'height': 14 })
            dot.eq(5).children().children().css({ 'width': '14', 'height': 14, 'fill': '#E2E2E2' })
            dot.eq(4).children().css({ 'width': '14', 'height': 14 })
            dot.eq(4).children().children().css({ 'width': '14', 'height': 14, 'fill': '#E2E2E2' })
            dot.eq(3).children().css({ 'width': '14', 'height': 14 })
            dot.eq(3).children().children().css({ 'width': '14', 'height': 14, 'fill': '#E2E2E2' })
            dot.eq(1).children().css({ 'width': '14', 'height': 14 })
            dot.eq(1).children().children().css({ 'width': '14', 'height': 14, 'fill': '#E2E2E2' })
          }
          else if (window.scrollY >= 2500 && window.scrollY < 3332) {
            dot.eq(0).children().css({ 'width': '14', 'height': 14 })
            dot.eq(0).children().children().css({ 'width': '14', 'height': 14, 'fill': '#E2E2E2' })
            dot.eq(3).children().css({ 'width': '18', 'height': 18 })
            dot.eq(3).children().children().css({ 'width': '18', 'height': 18, 'fill': '#CFCFCF' })
            dot.eq(6).children().css({ 'width': '14', 'height': 14 })
            dot.eq(6).children().children().css({ 'width': '14', 'height': 14, 'fill': '#E2E2E2' })
            dot.eq(5).children().css({ 'width': '14', 'height': 14 })
            dot.eq(5).children().children().css({ 'width': '14', 'height': 14, 'fill': '#E2E2E2' })
            dot.eq(4).children().css({ 'width': '14', 'height': 14 })
            dot.eq(4).children().children().css({ 'width': '14', 'height': 14, 'fill': '#E2E2E2' })
            dot.eq(2).children().css({ 'width': '14', 'height': 14 })
            dot.eq(2).children().children().css({ 'width': '14', 'height': 14, 'fill': '#E2E2E2' })
            dot.eq(1).children().css({ 'width': '14', 'height': 14 })
            dot.eq(1).children().children().css({ 'width': '14', 'height': 14, 'fill': '#E2E2E2' })
          }
          else if (window.scrollY >= 3333 && window.scrollY < 4214) {
            dot.eq(0).children().css({ 'width': '14', 'height': 14 })
            dot.eq(0).children().children().css({ 'width': '14', 'height': 14, 'fill': '#E2E2E2' })
            dot.eq(4).children().css({ 'width': '18', 'height': 18 })
            dot.eq(4).children().children().css({ 'width': '18', 'height': 18, 'fill': '#CFCFCF' })
            dot.eq(6).children().css({ 'width': '14', 'height': 14 })
            dot.eq(6).children().children().css({ 'width': '14', 'height': 14, 'fill': '#E2E2E2' })
            dot.eq(5).children().css({ 'width': '14', 'height': 14 })
            dot.eq(5).children().children().css({ 'width': '14', 'height': 14, 'fill': '#E2E2E2' })
            dot.eq(3).children().css({ 'width': '14', 'height': 14 })
            dot.eq(3).children().children().css({ 'width': '14', 'height': 14, 'fill': '#E2E2E2' })
            dot.eq(2).children().css({ 'width': '14', 'height': 14 })
            dot.eq(2).children().children().css({ 'width': '14', 'height': 14, 'fill': '#E2E2E2' })
            dot.eq(1).children().css({ 'width': '14', 'height': 14 })
            dot.eq(1).children().children().css({ 'width': '14', 'height': 14, 'fill': '#E2E2E2' })
          }
          else if (window.scrollY >= 4214 && window.scrollY < 5030) {
            dot.eq(0).children().css({ 'width': '14', 'height': 14 })
            dot.eq(0).children().children().css({ 'width': '14', 'height': 14, 'fill': '#E2E2E2' })
            dot.eq(5).children().css({ 'width': '18', 'height': 18 })
            dot.eq(5).children().children().css({ 'width': '18', 'height': 18, 'fill': '#CFCFCF' })
            dot.eq(6).children().css({ 'width': '14', 'height': 14 })
            dot.eq(6).children().children().css({ 'width': '14', 'height': 14, 'fill': '#E2E2E2' })
            dot.eq(4).children().css({ 'width': '14', 'height': 14 })
            dot.eq(4).children().children().css({ 'width': '14', 'height': 14, 'fill': '#E2E2E2' })
            dot.eq(3).children().css({ 'width': '14', 'height': 14 })
            dot.eq(3).children().children().css({ 'width': '14', 'height': 14, 'fill': '#E2E2E2' })
            dot.eq(2).children().css({ 'width': '14', 'height': 14 })
            dot.eq(2).children().children().css({ 'width': '14', 'height': 14, 'fill': '#E2E2E2' })
            dot.eq(1).children().css({ 'width': '14', 'height': 14 })
            dot.eq(1).children().children().css({ 'width': '14', 'height': 14, 'fill': '#E2E2E2' })
          }
          else if (window.scrollY >= 5031) {
            dot.eq(0).children().css({ 'width': '14', 'height': 14 })
            dot.eq(0).children().children().css({ 'width': '14', 'height': 14, 'fill': '#E2E2E2' })
            dot.eq(6).children().css({ 'width': '18', 'height': 18 })
            dot.eq(6).children().children().css({ 'width': '18', 'height': 18, 'fill': '#CFCFCF' })
            dot.eq(5).children().css({ 'width': '14', 'height': 14 })
            dot.eq(5).children().children().css({ 'width': '14', 'height': 14, 'fill': '#E2E2E2' })
            dot.eq(4).children().css({ 'width': '14', 'height': 14 })
            dot.eq(4).children().children().css({ 'width': '14', 'height': 14, 'fill': '#E2E2E2' })
            dot.eq(3).children().css({ 'width': '14', 'height': 14 })
            dot.eq(3).children().children().css({ 'width': '14', 'height': 14, 'fill': '#E2E2E2' })
            dot.eq(2).children().css({ 'width': '14', 'height': 14 })
            dot.eq(2).children().children().css({ 'width': '14', 'height': 14, 'fill': '#E2E2E2' })
            dot.eq(1).children().css({ 'width': '14', 'height': 14 })
            dot.eq(1).children().children().css({ 'width': '14', 'height': 14, 'fill': '#E2E2E2' })
          }

        }
      }

      window.addEventListener('scroll', debounce(checkSlide));

      dot_holder.html(content);
    });
  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired

    /* eslint-disable */

    let projectForm = $('#upload-cv-form');

    projectForm.on('submit', (e) => {
      e.preventDefault();

      let client_name = document.querySelector('#upload-cv-form #client-name').value;
      let phone_num = document.querySelector('#upload-cv-form #client-phone-num').value;
      let email_addr = document.querySelector('#upload-cv-form #client-email-addr').value;
      let cv = document.querySelector('#upload-cv-form #client-cv').value;
      let hutk = document.cookie.split('; ')
                                .find(row => row.startsWith('hubspotutk='))
                                .split('=')[1]
                                .toString();

      let sendInfo = {
        "fields": [
          {
            "name": "email",
            "value": email_addr
          },
          {
            "name": "firstname",
            "value": client_name
          },
          {
            "name": "phone",
            "value": phone_num
          },
          {
            "name": "projectpdf",
            "value": cv,
            "fieldType": "file"
          }
        ],
        "context": {
          "pageUri": window.location.toString(),
          "hutk": hutk,
          "pageName": location.href.split("/").slice(-1).toString()
        }
      }

      $.ajax({
        url: "https://api.hsforms.com/submissions/v3/integration/submit/4773320/1067c223-8559-4293-a2dc-e6e376bdb28c",
        method: "POST",
        dataType: "json",
        data: JSON.stringify(sendInfo),
        contentType: "application/json",

        success: () => {
          window.location.replace("https://alphaefficiency.com/thank-you-page");
        },
        err: (err, xhr) => {
          console.log(err, xhr);
        }
      })
    });

    $('carouosel-control-prev, .carousel-control-next').on('click', e => {
      e.preventDefault()
      });

//       var select = document.getElementById('mySelect');
// select.onchange = function () {
//     select.className = 'redText';
// }

// $(".checkbox-dropdown").click(function () {
//   $(this).toggleClass("is-active");
// });

// $(".checkbox-dropdown ul").click(function(e) {
//   e.stopPropagation();
// });

$(".rotate").click(function(){
  $(this).toggleClass("down")  ; 
 })
  },
};