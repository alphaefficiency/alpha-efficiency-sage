{{--
  Template Name: Careers Developer
--}}

@extends('layouts.app')

@section('content')

  @include('partials.careers-dev.hero')

  @include('partials.careers-dev.wrepper')
  
  @include('partials.careers.career-single-grow')

  @include('partials.careers.career-single-form')

@endsection