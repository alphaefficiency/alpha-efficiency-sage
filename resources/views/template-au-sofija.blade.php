{{--
  Template Name: About - Sofija
--}}

@extends('layouts.app')

@section('content')

  @include('partials.about-single.au-sofija.hero-content')

  @include('partials.about-single.au-sofija.main-content')

@endsection