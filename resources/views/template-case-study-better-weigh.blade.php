{{--
  Template Name: Case Study Better Weigh
--}}

@extends('layouts.app')

@section('content')

@include('partials.better-weigh.hero-study')
@include('partials.better-weigh.img-surge')
@include('partials.better-weigh.usability-section')
@include('partials.better-weigh.problem-section')
@include('partials.better-weigh.solution-section')
@include('partials.better-weigh.results-section')
@include('partials.better-weigh.comment-section')
@include('partials.better-weigh.form-section')
@endsection
