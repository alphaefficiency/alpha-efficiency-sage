{{--
  Template Name: Case Study Aeriform
--}}

@extends('layouts.app')

@section('content')

@include('partials.aeriform.hero-study')
@include('partials.aeriform.img-surge')
@include('partials.aeriform.usability-section')
@include('partials.aeriform.problem-section')
@include('partials.aeriform.solution-section')
@include('partials.aeriform.results-section')
@include('partials.aeriform.comment-section')
@include('partials.aeriform.form-section')
@endsection
