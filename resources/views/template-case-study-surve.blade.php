{{--
  Template Name: Case Study Surve
--}}

@extends('layouts.app')

@section('content')

@include('partials.surve.hero-study')
@include('partials.surve.img-surge')
@include('partials.surve.usability-section')
@include('partials.surve.problem-section')
@include('partials.surve.solution-section')
@include('partials.surve.results-section')
@include('partials.surve.comment-section')
@include('partials.surve.form-section')
@endsection
