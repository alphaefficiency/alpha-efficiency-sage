{{--
  Template Name: Case Study Academic Trading
--}}

@extends('layouts.app')

@section('content')

@include('partials.academic-trading.hero-study')
@include('partials.academic-trading.img-surge')
@include('partials.academic-trading.usability-section')
@include('partials.academic-trading.problem-section')
@include('partials.academic-trading.solution-section')
@include('partials.academic-trading.results-section')
@include('partials.academic-trading.comment-section')
@include('partials.academic-trading.form-section')
@endsection
