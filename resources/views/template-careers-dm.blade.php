{{--
  Template Name: Careers Digital Marketer
--}}

@extends('layouts.app')

@section('content')

  @include('partials.careers-dm.hero')

  @include('partials.careers-dm.wrepper')
  
  @include('partials.careers.career-single-grow')

  @include('partials.careers.career-single-form')

@endsection