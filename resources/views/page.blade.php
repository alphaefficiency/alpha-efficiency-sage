@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.page-header')
    <div class="content-holder">
      @include('partials.content-page')
    </div>
  @endwhile
  @include('partials/blog/blog-recent')
@endsection
