{{--
  Template Name: Hiring Template
--}}

@extends('layouts.app')

@section('content')
  @include('partials.hiring.header')
  @include('partials.hiring.offer')
  @include('partials.hiring.benefits')
  @include('partials.hiring.careers')
@endsection