{{--
  Template Name: Case Study Surge Strength
--}}

@extends('layouts.app')

@section('content')

@include('partials.case-study-surge.hero-study')
@include('partials.case-study-surge.img-surge')
@include('partials.case-study-surge.usability-section')
@include('partials.case-study-surge.problem-section')
@include('partials.case-study-surge.solution-section')
@include('partials.case-study-surge.results-section')
@include('partials.case-study-surge.comment-section')
@include('partials.case-study-surge.form-section')
@endsection
