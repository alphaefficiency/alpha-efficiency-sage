<section class="last">
  <div class="lines-bg">

  </div>
  <div class="container-fluid">
    <div class="top">
      <div class="lefts">
        <h1 class="proven"><b>Want to start your creative project today?</b> Fill out this form,  and let’s discuss your next steps.</h1>
      </div>
    </div>

    <div class="bottom">
      <h2>Tell Us About Your Project.</h2>
      <hr>
      <?= do_shortcode('[contact-form-7 id="14164" title="Have A Project To Do"]'); ?>
    </div>
  </div>
</section>