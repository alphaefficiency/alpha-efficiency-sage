<div class="hero-content">
  <div class="info-content">
    <div class="txt">
      <div class="img-wrapper">
        <div class="img"></div>
      </div>
      <h1>Diego Espinoza</h1>
      <h2>Email Marketing Manager</h2>
      <div class="line"></div>
      <p>As a proven strategist and creative professional, Diego establishes standards and procedures that enable our departmental teams to operate as efficiently as possible. Diego’s result-oriented approach is structured around client performance, fostering win-win scenarios that promote client satisfaction. Outside of the office, he enjoys spending time with his family and watching Chicago-based sports teams.</p>
    </div>
  </div>
</div>  
