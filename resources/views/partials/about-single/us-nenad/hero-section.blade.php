<div class="hero-content">
  <div class="info-content">
    <div class="txt">
      <div class="img-wrapper">
        <div class="img"></div>
      </div>
      <h1>Nenad Nidzovic</h1>
      <h2>Content Writer</h2>
      <div class="line"></div>
      <p>Nenad is an advanced literature student with a passion for wordsmithing. He is thrilled every time he comes across a peculiar word or meaning which he writes down in his dictionary as he constantly strives to better his command of the English language.</p>
    </div>
  </div>
</div>  
