<div class="hero-content">
  <div class="info-content">
    <div class="txt">
      <div class="img-wrapper">
        <div class="img"></div>
      </div>
      <h1>Branislav Jeftic</h1>
      <h2>Marketing</h2>
      <div class="line"></div>
      <p>Bane is a prolific PPC advertiser and expert in SEO and email marketing. He has a keen eye for keyword research and manages to find new and easily overlooked opportunities to drive traffic to our website. When a hard day’s work comes to an end, he likes to relax with a bag of popcorn and a movie. </p>
    </div>
  </div>
</div>  
