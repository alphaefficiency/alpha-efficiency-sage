<div class="hero-content">
  <div class="info-content">
    <div class="txt">
      <div class="img-wrapper">
        <div class="img"></div>
      </div>
      <h1>Sara Grubesevic</h1>
      <h2>Content Writer</h2>
      <div class="line"></div>
      <p>Sara joined Alpha as a content writer intern, but she quickly showed great interest in writing high-quality SEO-optimized content. Her desire to learn new things and improve her performance contributed to her becoming our content editor and assisting our writers in creating outstanding content.</p>
    </div>
  </div>
</div>  
