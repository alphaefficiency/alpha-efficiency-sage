<div class="hero-content">
  <div class="info-content">
    <div class="txt">
      <div class="img-wrapper">
        <div class="img"></div>
      </div>
      <h1>Tijana Levicar</h1>
      <h2>Executive Assistant</h2>
      <div class="line"></div>
      <p>Tijana does most of our backlinks research and cheks, as well as assists with emailing and negotiations. Although she joined Alpha with little experience, her great ambition, curiosity, and desire to learn made her an irreplaceable part of our marketing team.</p>
    </div>
  </div>
</div>  
