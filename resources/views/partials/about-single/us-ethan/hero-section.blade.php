<div class="hero-content">
  <div class="info-content">
    <div class="txt">
      <div class="img-wrapper">
        <div class="img"></div>
      </div>
      <h1>Ethan Johnson</h1>
      <h2>Sales</h2>
      <div class="line"></div>
      <p>Ethan is our business development manager who has successfully operated and executed numerous sales strategies and contributed to our client satisfaction. His ability to translate problems into opportunities accompanied by strong computer and technical skills made him an indispensable part of our team.</p>
    </div>
  </div>
</div>  
