<div class="hero-content">
  <div class="info-content">
    <div class="txt">
      <div class="img-wrapper">
        <div class="img"></div>
      </div>
      <h1>Teodora Markovic</h1>
      <h2>Executive Assistant</h2>
      <div class="line"></div>
      <p>Teodora is an executive assistant at Alpha Efficiency. Her excellent time management and organizational skills have greatly contributed to our team. Her ability to multitask, combined with great attention to detail, makes it impossible for the omissions to go unnoticed.</p>
    </div>
  </div>
</div>  
