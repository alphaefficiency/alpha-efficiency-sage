<div class="hero-content">
  <div class="info-content">
    <div class="txt">
      <div class="img-wrapper">
        <div class="img"></div>
      </div>
      <h1>Sofija Sokic</h1>
      <h2>Web Developer</h2>
      <div class="line"></div>
      <p>Sofija is interested in front end development, user experience and everything in it’s orbit. She’s been fascinated by web design and building websites. One of the reasons she decided to become a developer is because of the satisfaction that comes from creating something that is visually tangible and has the potential to contribute to the bottom line and results.</p>
      <div class="svg">
        <a href="https://www.linkedin.com/in/sofija-sokic-1a3abb124/" target="_blank"><div class="insta">
         <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
           <path d="M4.72631 19.1894H0.765156V6.43364H4.72631V19.1894ZM2.74334 4.69366C1.47687 4.69366 0.449219 3.64421 0.449219 2.37778C0.449219 1.76936 0.690921 1.18587 1.12115 0.75565C1.55138 0.325433 2.1349 0.0837402 2.74334 0.0837402C3.35178 0.0837402 3.9353 0.325433 4.36553 0.75565C4.79577 1.18587 5.03747 1.76936 5.03747 2.37778C5.03747 3.64421 4.00982 4.69366 2.74334 4.69366ZM19.5515 19.1894H15.5992V12.9801C15.5992 11.5 15.5691 9.60245 13.5398 9.60245C11.4804 9.60245 11.1645 11.21 11.1645 12.8736V19.1894H7.2074V6.43364H11.0061V8.17362H11.0614C11.5903 7.17125 12.882 6.11362 14.809 6.11362C18.8179 6.11362 19.5549 8.75361 19.5549 12.1824V19.1894H19.5515Z" fill="#9D9BA3"/>
         </svg>  
         </div></a> 
       </div>
    </div>
  </div>
</div>  
