<article {{ post_class() }}>
  <div class="hero">
    <div class="title-holder">
      <p class="title">{!! get_the_title() !!}</p>
    </div>
  </div>
  <div class="container-fluid">
    {{-- <a href="{{ get_site_url() }}/schedule-a-call">
    <div class="mobile-join"></div>
    </a> --}}
    <div class="content">
      <div class="entry-content">
        <div class="content1">
            @php the_content() @endphp
        </div>
      </div>
      <div class="author-card">
        <div class="image">
          <img src="@asset('images/about/bojan.png')" width="93" height="93" alt="Brian Djordjevic" class="img-fluid">
        </div>
        <div class="content">
          <small>About The Author</small>
          <h3>Brian Dordevic</h3>
          <p class="text-author">
            Brian is Marketing Strategic Planner with a passion for all things digital. Feel free to follow
            him on <a href="https://twitter.com/briandordevic" target="_blank">Twitter</a> or <a
              href="{{ get_site_url() }}/contact">schedule a consultation call with him</a>.
          </p>
        </div>
      </div>
      <div class="related">
      </div>
      @php comments_template('/partials/comments.blade.php') @endphp
    </div>
    <div class="right">
      <a href="{{ get_site_url() }}/schedule-a-call" class="marketing-audit"></a>
      <div class="first-issue-image">
        <a href="{{ get_site_url() }}/website-grader">
          <img src="@asset('images/blog/grader.svg')" width="330" height="152" alt="Grader" />
        </a>
      </div>
      <p class="platinum-text">
        Obtain our report that contains manual user testing insights from the team of seasoned QA experts. Don’t
        leave your website performance in disarray.
      </p>

      <div class="recent-posts">
        <p>Recent Posts</p>
        <hr>
        @php
        $recent_posts = wp_get_recent_posts([
        'numberposts' => 3,
        'post_status' => 'publish',
        ]);
        @endphp
        <ul>
          @foreach ($recent_posts as $post)
          <li><a href="<?php echo get_permalink($post['ID']); ?>" class="read-more">
              {{ $post['post_title'] }}
            </a></li>
          @endforeach
        </ul>
      </div>
      <div class="recent-posts mt-5">
        <p>#trending</p>
        <hr>
        @php
        $trending_posts = get_posts([
        'numberposts' => 8,
        'meta_key' => 'trending',
        'meta_value' => 1,
        ]);
        @endphp
        <ul>
          @foreach ($trending_posts as $post)
          <li><a href="<?php echo get_permalink($post->ID); ?>" class="read-more">
              {{ $post->post_title }}
            </a></li>
          @endforeach
        </ul>
      </div>
    </div>
  </div>
</article>
<div class="log">
  <div class="box">
    <div class="ask">
      <h1><strong>Want to start your creative project today?</strong> Fill out this form, and let’s discuss your next
        steps.</h1>
    </div>
    <div class="about-form">
      @component('components.forms.project', ['title' => "Tell Us About Your Project."]) @endcomponent
    </div>
  </div>
</div>
