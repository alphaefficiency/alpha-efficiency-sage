<h1>Website Grader</h1>
<h2>Grade your website in seconds. Then learn how to improve it</h2>
{{-- <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/shell.js"></script>
<script>
    hbspt.forms.create({
        region: "na1",
        portalId: "4773320",
        formId: "bd511b9d-10d9-4dd5-8c42-6d64fbfa41ba"
    });
</script> --}}
@php the_content() @endphp

<div class="cover"></div>