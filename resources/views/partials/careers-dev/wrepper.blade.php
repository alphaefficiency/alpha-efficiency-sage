<div class="wrepper" id="section2">
  <div class="left">
    <h2>Apply now and join the <br> <strong> Alpha Efficiency</strong> Crew.</h2>
    <div class="line"></div>
  </div>
  <div class="right">
    <div class="line"></div>
    <div class="ul">    
     <div class="svg">
    <h2>If you...</h2>
        <svg id="up" width="22" height="13" viewBox="0 0 22 13" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M21 12L11 2L1 12" stroke="#1994FF" stroke-width="2"/>
        </svg>          
        <svg id="down" width="22" height="13" viewBox="0 0 22 13" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M21 1L11 11L1 0.999998" stroke="#1994FF" stroke-width="2"/>
        </svg>          
      </div>
      <ul id="ul">
        <li>Have experience translating and writing digital content to an exceptionally high standard</li>
        <li>Have experience in Microsoft Word, Excel</li>
        <li>Write and speak fluent English and have amazing communication skills</li>
        <li>Really care about quality and efficiency of your work</li>
        <li>Are a quick learner with ability to solve problems</li>
        <li>Have ability to work with constant interruptions and multitask</li>
        <li>Have ability to meet deadlines and work professionally under pressure</li>
      </ul>
   </div>
   <div class="line" id="line"></div>
   <div class="line"></div>
   <div class="if">
    <div class="svg">
      <h2>You get...</h2>
          <svg id="up2" width="22" height="13" viewBox="0 0 22 13" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M21 12L11 2L1 12" stroke="#1994FF" stroke-width="2"/>
          </svg>          
          <svg id="down2" width="22" height="13" viewBox="0 0 22 13" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M21 1L11 11L1 0.999998" stroke="#1994FF" stroke-width="2"/>
          </svg>          
        </div>
      <ul id="ul2">
          <li>Competitive salary and performance bonuses upon successful project completion</li> 
          <li>To Solve problems in time efficient manner and think like there is no box</li> 
          <li>To Work with a marketing & development team that is determined to win for their clients</li> 
        <li>To work on a part-time to full time position</li> 
        <li>To work from home</li> 
      </ul>
  </div>
  <div class="line"></div>
  </div>
</div>