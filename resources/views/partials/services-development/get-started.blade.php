<section class="get-started">
    <div class="started-box" style="max-width: 950px;">
        <p>Code runs your business, and having a trustworthy partner is valuable. Alpha Efficiency’s team will help you maintain your tech infrastructure that powers your business. This leaves you time to do what you do best: <br style="display:none;"/><strong>Run your business.</strong><span class="hidden-service" style="color:#1994ff; margin-left:5px;">Read More</span></p>
        <p class="hidden-content-service">Let’s start by auditing your technology stack, seeing reality on what needs to be taken care of.</p>
        <a href="{{ get_site_url() }}/schedule-a-call" class="get-started-btn">Get Started</a>
    </div>
</section>