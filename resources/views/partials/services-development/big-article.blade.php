<div class="redesign-wrap">
    <div class="paid-redesign2">
        <div class="Retxt">
          <h1>Lorem Ipsum is Simply Dummy Text </h1>
          <div class="line"></div>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut convallis mauris sodales enim suscipit ornare ut sed lorem. Integer et elit vel ipsum malesuada vulputate. Quisque hendrerit condimentum iaculis. Etiam ut arcu quam. Nunc dignissim diam accumsan, rhoncus sem ut, varius magna. Praesent lacinia a libero eu eleifend. Sed sit amet massa fringilla, maximus neque eget, molestie turpis. Nullam vestibulum sapien orci, et condimentum justo fringilla nec. Proin consequat tellus et porta porttitor. Nullam a tortor lectus. Pellentesque pos`uere euismod luctus. Nunc a diam ac metus scelerisque sodales. Aenean venenatis turpis vel elit pulvinar faucibus. Mauris cursus lacus dui, eu gravida felis venenatis nec. Phasellus ac lorem vulputate, egestas tortor ac, placerat orci. Aliquam vitae sem nec est porta pulvinar vitae quis leo.</p>
        </div>
        <div class="Reimg"></div>
        <div class="Retxt">
          <h2 class="h2">Lorem Ipsum</h2>
          <p>Phasellus volutpat vestibulum tortor vitae congue. Sed quis quam ac neque egestas varius. Phasellus a tempor est. Proin ante nulla, euismod at elit a, luctus laoreet risus. Donec sodales molestie magna, a sodales enim interdum ut. Fusce varius enim at orci ultrices, id ullamcorper lacus aliquam. Vestibulum ac lectus et nibh vehicula semper ac vitae libero. Ut ut sagittis justo. Etiam ut vulputate velit. Suspendisse scelerisque dictum eros. Nunc vulputate porta nibh, sed ullamcorper purus ultrices et. Sed nec malesuada lorem. Proin erat risus, pharetra quis ligula feugiat, maximus suscipit lectus. Aenean neque leo, mattis euismod condimentum ut, tincidunt a diam.</p>
        </div>
      </div>
      <div class="paid-redesign3">
        <div class="article-frame counter-art">
          <div class="img-wrap">
              <img src="@asset('images/services/development/redesign-article.png')" alt="Article" class="img-fluid"/>
          </div>
          <div class="art-txt">
              <h4>Winning The Game of Inches</h4>
              <div class="line"></div>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut convallis mauris sodales enim suscipit ornare ut sed lorem. Integer et elit vel ipsum malesuada vulputate.</p>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
          </div>
        </div>
      </div>
      
      <div class="paid-redesign4">
        <div class="txt">
          <h1>Lorem Ipsum</h1> <br><br>
          <div class="p">
            <p>Quisque ac mattis sapien, ac egestas lorem. Praesent erat mi, dapibus sagittis urna eget, blandit finibus augue. Nullam mauris nisi, vulputate in consequat hendrerit, rutrum ac velit. Quisque lacinia, ante eget consectetur dictum, est velit commodo libero, cursus tincidunt erat lectus sit amet diam. Cras ut nulla ut lacus lacinia molestie tincidunt aliquet ipsum. Sed tempus pretium sem, in vehicula libero pulvinar finibus. Mauris posuere imperdiet</p>
            <p> fermentum. Suspendisse potenti. Maecenas vel libero nisl. In quis tortor euismod, faucibus velit non, condimentum nisl. Vivamus quam sem, ultrices in mauris ut, tincidunt suscipit sapien. Nullam iaculis mauris nec felis dapibus, <br> nec maximus nibh convallis. In posuere at lectus eu pulvinar. Aliquam nec sapien dignissim, varius odio ut, porttitor enim. Aenean porttitor purus quis urna imperdiet tempus. Nam aliquet nec sem ut tristique. Donec a ornare leo. Vestibulum ornare consectetur sem, quis eleifend massa. Mauris hendrerit molestie urna, ac aliquam dui fermentum vel. Proin vehicula orci a malesuada pellentesque. Cras nec tristique arcu, pulvinar laoreet augue. Integer mattis risus felis, eget bibendum mauris facilisis ac. Nullam </p>
            <p>sollicitudin orci ac nibh venenatis mollis. <br> Suspendisse ullamcorper rhoncus eros, nec tincidunt urna pellentesque eu. Proin sit amet malesuada ex, ut congue orci. Praesent et pellentesque nisi. Duis elementum elementum enim vitae maximus. Curabitur at nibh a tortor tincidunt fringilla in a lorem. Sed ut orci id lacus fringilla iaculis nec aliquam ex. Mauris tristique egestas efficitur. In hac habitasse platea dictumst. Donec lacinia mauris augue, at euismod lacus porta eget.</p>
          </div>
        </div>
      </div>
      <div class="paid-redesign5">
        <div class="hidd txt">
          <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Voluptatibus sapiente dignissimos aliquam omnis velit aliquid facilis excepturi quod beatae numquam itaque, eum eveniet fugit hic vel libero. Minima et vel, commodi aperiam consequatur suscipit beatae accusamus sint provident, reprehenderit minus, possimus velit aliquid. Expedita laudantium quam exercitationem. Cum, harum consectetur.</p>
        </div>
        <div class="btn" id="btn">
          <svg class="svg1" width="39" height="39" viewBox="0 0 39 39" fill="none" xmlns="http://www.w3.org/2000/svg">
            <circle cx="19.5" cy="19.5" r="18.5" stroke="#1994FF" stroke-width="2" />
            <path d="M19.9331 10.4001V27.7335" stroke="#1994FF" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
            <path d="M11.2666 19.0668L28.5999 19.0668" stroke="#1994FF" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
          </svg>
          <svg class="svg2" width="39" height="39" viewBox="0 0 39 39" fill="none" xmlns="http://www.w3.org/2000/svg">
            <circle cx="19.5" cy="19.5" r="18.5" stroke="#1994FF" stroke-width="2" />
            <path d="M11.2666 19.0669L28.5999 19.0669" stroke="#1994FF" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
          </svg>
        </div>
        <div class="txt">
          <p id="ch">See More</p>
        </div>
      </div>
  </div>