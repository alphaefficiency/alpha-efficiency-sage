<section class="service-header">
    <div class="tint">
        <div class="header-box">
            <svg width="140" height="140" viewBox="0 0 140 140" fill="none" xmlns="http://www.w3.org/2000/svg">
                <rect width="140" height="140" rx="70" fill="#1994FF"/>
                <circle cx="90.7918" cy="49.2078" r="6.20779" stroke="white" stroke-width="4"/>
                <circle cx="49.2078" cy="90.7937" r="6.20779" stroke="white" stroke-width="4"/>
                <path d="M83.5088 86.6079C83.5088 84.3883 85.3082 82.5889 87.5278 82.5889C89.7475 82.5889 91.5469 84.3883 91.5469 86.6079C91.5469 88.8276 89.7475 90.627 87.5278 90.627C85.3082 90.627 83.5088 88.8276 83.5088 86.6079Z" fill="white" stroke="white" stroke-width="4"/>
                <path d="M51.5888 52.9695L60.5996 61.7436" stroke="white" stroke-width="3"/>
                <path d="M86.0209 84.9071L76.9071 76.1783" stroke="white" stroke-width="3"/>
                <path d="M53.0371 86.9646L62.8865 77.1152" stroke="white" stroke-width="3"/>
                <path d="M77.1152 62.8884L86.9646 53.0391" stroke="white" stroke-width="3"/>
                <circle cx="68.9062" cy="68.9062" r="11.6796" stroke="white" stroke-width="4"/>
                <circle cx="50.2064" cy="51.1263" r="6.44463" fill="white"/>
            </svg>                                
            <h1>Social Media</h1>     
            <p>Built for Brand Awareness</p>        
        </div>
    </div>
</section>