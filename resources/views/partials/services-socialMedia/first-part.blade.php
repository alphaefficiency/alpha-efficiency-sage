<div class="service-first-part">
    <section class="get-started" style="background:#f9f9f9;">
        <div class="started-box" style="max-width: 880px;">
            <div class="p-box" style="display: flex; justify-content: space-between;">
                <p>Your paid search performance is about to take off. <strong>6 million dollars on ad spend experience tells you you’re on the right page.</strong> Alpha Efficiency’s team has been successfully executing lead generation and search marketing campaigns for the past decade, and now it’s time for your business to experience what ROI positive means.<span class="hidden-service" style="color:#1994ff; margin-left:5px;">Read More</span></p>
                <p class="hidden-content-service"><strong>Work with Talent</strong> ranging from top performing user experience designers to high performance web analysts that will get the returns you deserve. Empower your business with next level search technology that cuts through the noise and reduces your lead cost in return improving your bottom line.</p>
            </div>
            <a href="{{ get_site_url() }}/schedule-a-call" class="get-started-btn">Get Started</a>
        </div>
    </section>
    <div class="quotes">
        <div class="quotes-box" style="max-width:660px;">
            <svg width="40" height="35" viewBox="0 0 40 35" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M14.3956 3.02469C12.9304 4.89712 11.5018 7.52572 10.1099 10.9105C8.71795 14.2233 8.05861 17.284 8.13187 20.0926C8.79121 19.8765 9.56044 19.7685 10.4396 19.7685C12.5641 19.7685 14.359 20.3807 15.8242 21.6049C17.2894 22.7572 18.022 24.5216 18.022 26.8981C18.022 29.2027 17.2528 31.1471 15.7143 32.7315C14.2491 34.2438 12.3077 35 9.89011 35C6.73993 35 4.28571 33.9558 2.52747 31.8673C0.842491 29.7068 0 26.9702 0 23.6574C0 20.1286 0.842491 16.3117 2.52747 12.2068C4.28571 8.10185 6.81319 4.03292 10.1099 0L14.3956 3.02469ZM36.3736 3.02469C34.9084 4.89712 33.4799 7.52572 32.0879 10.9105C30.696 14.2233 30.0366 17.284 30.1099 20.0926C30.7692 19.8765 31.5385 19.7685 32.4176 19.7685C34.5421 19.7685 36.337 20.3807 37.8022 21.6049C39.2674 22.7572 40 24.5216 40 26.8981C40 29.2027 39.2308 31.1471 37.6923 32.7315C36.2271 34.2438 34.2857 35 31.8681 35C28.718 35 26.2637 33.9558 24.5055 31.8673C22.8205 29.7068 21.978 26.9702 21.978 23.6574C21.978 20.1286 22.8205 16.3117 24.5055 12.2068C26.2637 8.10185 28.7912 4.03292 32.0879 0L36.3736 3.02469Z" fill="#DFDFDF"/>
            </svg>
            <p>Social media usage is intense and has people addicted to their phones. Stay on top of their mind at the time of their leisure.</p>
            <svg width="40" height="35" viewBox="0 0 40 35" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M25.6044 31.9753C27.0696 30.1029 28.4982 27.4743 29.8901 24.0895C31.2821 20.7767 31.9414 17.716 31.8681 14.9074C31.2088 15.1235 30.4396 15.2315 29.5604 15.2315C27.4359 15.2315 25.641 14.6193 24.1758 13.3951C22.7106 12.2428 21.978 10.4784 21.978 8.10185C21.978 5.79733 22.7473 3.85288 24.2857 2.26852C25.7509 0.756171 27.6923 2.73872e-06 30.1099 2.95008e-06C33.2601 3.22547e-06 35.7143 1.04424 37.4725 3.13272C39.1575 5.29321 40 8.02984 40 11.3426C40 14.8714 39.1575 18.6883 37.4725 22.7932C35.7143 26.8981 33.1868 30.9671 29.8901 35L25.6044 31.9753ZM3.62637 31.9753C5.09157 30.1029 6.52014 27.4743 7.91208 24.0895C9.30403 20.7767 9.96337 17.716 9.89011 14.9074C9.23077 15.1235 8.46154 15.2315 7.58241 15.2315C5.45787 15.2315 3.663 14.6193 2.1978 13.3951C0.732599 12.2428 -1.67095e-06 10.4784 -1.46319e-06 8.10185C-1.26172e-06 5.79733 0.769233 3.85288 2.30769 2.26851C3.77289 0.756169 5.71429 8.17345e-07 8.13187 1.0287e-06C11.2821 1.30409e-06 13.7363 1.04424 15.4945 3.13272C17.1795 5.29321 18.022 8.02983 18.022 11.3426C18.022 14.8714 17.1795 18.6883 15.4945 22.7932C13.7363 26.8981 11.2088 30.9671 7.91208 35L3.62637 31.9753Z" fill="#DFDFDF"/>
            </svg>                               
        </div>
    </div>
    <section class="first-article">
        <div class="article-frame">
            <div class="art-txt">
                <h4>Are you visible?</h4>
                <div class="line"></div>
                <p>Does your brand even exist if it is not seen? Our mindshare is a critical component of our shared perception. Reality is where the eye balls are, and your business has the opportunity to purchase this attention, guide it and influence it. Ready?</p>
                <a href="{{ get_site_url() }}/schedule-a-call">Schedulle a Call</a>
            </div>
            <div class="img-wrap">
                <img src="@asset('images/services/social-media/first-art11.png')" alt="Article" class="img-fluid"/>
            </div>
        </div>
    </section>
    <div class="quotes">
        <div class="quotes-box" style="max-width: 580px;">
            <svg width="40" height="35" viewBox="0 0 40 35" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M14.3956 3.02469C12.9304 4.89712 11.5018 7.52572 10.1099 10.9105C8.71795 14.2233 8.05861 17.284 8.13187 20.0926C8.79121 19.8765 9.56044 19.7685 10.4396 19.7685C12.5641 19.7685 14.359 20.3807 15.8242 21.6049C17.2894 22.7572 18.022 24.5216 18.022 26.8981C18.022 29.2027 17.2528 31.1471 15.7143 32.7315C14.2491 34.2438 12.3077 35 9.89011 35C6.73993 35 4.28571 33.9558 2.52747 31.8673C0.842491 29.7068 0 26.9702 0 23.6574C0 20.1286 0.842491 16.3117 2.52747 12.2068C4.28571 8.10185 6.81319 4.03292 10.1099 0L14.3956 3.02469ZM36.3736 3.02469C34.9084 4.89712 33.4799 7.52572 32.0879 10.9105C30.696 14.2233 30.0366 17.284 30.1099 20.0926C30.7692 19.8765 31.5385 19.7685 32.4176 19.7685C34.5421 19.7685 36.337 20.3807 37.8022 21.6049C39.2674 22.7572 40 24.5216 40 26.8981C40 29.2027 39.2308 31.1471 37.6923 32.7315C36.2271 34.2438 34.2857 35 31.8681 35C28.718 35 26.2637 33.9558 24.5055 31.8673C22.8205 29.7068 21.978 26.9702 21.978 23.6574C21.978 20.1286 22.8205 16.3117 24.5055 12.2068C26.2637 8.10185 28.7912 4.03292 32.0879 0L36.3736 3.02469Z" fill="#DFDFDF"/>
            </svg>
            <p>Alpha Efficiency has a decade of experience with Facebook Ads, and we cover all major social media platforms.</p>
            <svg width="40" height="35" viewBox="0 0 40 35" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M25.6044 31.9753C27.0696 30.1029 28.4982 27.4743 29.8901 24.0895C31.2821 20.7767 31.9414 17.716 31.8681 14.9074C31.2088 15.1235 30.4396 15.2315 29.5604 15.2315C27.4359 15.2315 25.641 14.6193 24.1758 13.3951C22.7106 12.2428 21.978 10.4784 21.978 8.10185C21.978 5.79733 22.7473 3.85288 24.2857 2.26852C25.7509 0.756171 27.6923 2.73872e-06 30.1099 2.95008e-06C33.2601 3.22547e-06 35.7143 1.04424 37.4725 3.13272C39.1575 5.29321 40 8.02984 40 11.3426C40 14.8714 39.1575 18.6883 37.4725 22.7932C35.7143 26.8981 33.1868 30.9671 29.8901 35L25.6044 31.9753ZM3.62637 31.9753C5.09157 30.1029 6.52014 27.4743 7.91208 24.0895C9.30403 20.7767 9.96337 17.716 9.89011 14.9074C9.23077 15.1235 8.46154 15.2315 7.58241 15.2315C5.45787 15.2315 3.663 14.6193 2.1978 13.3951C0.732599 12.2428 -1.67095e-06 10.4784 -1.46319e-06 8.10185C-1.26172e-06 5.79733 0.769233 3.85288 2.30769 2.26851C3.77289 0.756169 5.71429 8.17345e-07 8.13187 1.0287e-06C11.2821 1.30409e-06 13.7363 1.04424 15.4945 3.13272C17.1795 5.29321 18.022 8.02983 18.022 11.3426C18.022 14.8714 17.1795 18.6883 15.4945 22.7932C13.7363 26.8981 11.2088 30.9671 7.91208 35L3.62637 31.9753Z" fill="#DFDFDF"/>
            </svg>                               
        </div>
    </div>
</div>