<section class="performance">
  <div class="container">
    <h6 class="site-title">Website Performance Made Beautiful</h6>
    <div class="iframe-wrap">
      <iframe src="https://www.youtube.com/embed/3L6mVMdHti4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
  </div>
</section>