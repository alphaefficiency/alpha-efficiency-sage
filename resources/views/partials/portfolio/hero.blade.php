<div class="hero">
  <div class="container">
    <h1>Get Inspired With <b><br/>Our Projects</b></h1>
  </div>
</div>
<div class="hero-badges">
  <div class="container">
    <img src="@asset('images/portfolio/badges.png')" alt="Badges" class="img-fluid"/>
  </div>
</div>