@php
  $details = ['duration' => 1, 'hours' => 221, 'members' => 4, 'link' => 'https://aeriformcinema.com/', 'demo' => 'Live'];   
@endphp

<section class="aeriform">
  <div class="container">
    <div class="img-wrap">
      <img src="@asset('images/portfolio/aeriform.png')" alt="Aeriform" class="img-fluid"/>
    </div>
    <div class="text-wrap white">
      <div class="inner">
        <h2 class='site-title'>Aeriform</h2>
        <div class="line"></div>
        <p>The main goal of <b>Aeriform Cinema</b> was achieving an improved look and performance. We’ve accomplished this by moving them off their Squarespace website onto Sage Framework, an enterprise-level WordPress solution. Since the focus was on a design, Alpha Efficiency’s in-house creatives worked on delivering an eye-catching and in-depth design.
        </p>
        @component('components.portfolio.details', ['details' => $details]) @endcomponent
      </div>
      <div class="actions">
        <a href="https://aeriformcinema.com/" target="_blank" class="demo">View Live</a>
        <div class="logo-frame-m">
          <img src="@asset('images/portfolio/logos/aeriform.svg')" alt="Aeriform Cinema Logo" class="img-fluid"/>
        </div>
        <div class="socials">
          <a href="https://www.behance.net/gallery/93492575/Aeriform-Cinema" target="_blank" rel="noopener noreferrer">
            <svg width="45" height="45" viewBox="0 0 45 45" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M18.1082 17.8297C18.1082 15.707 16.664 15.707 16.664 15.707H15.8994H10.7227V20.2618H16.2958C17.2588 20.2614 18.1082 19.9534 18.1082 17.8297Z" fill="#1994FF"/>
              <path d="M16.6645 23.4072H10.7227V28.8625H16.3901C17.2427 28.8381 18.8162 28.5711 18.8162 26.2108C18.8162 23.3787 16.6645 23.4072 16.6645 23.4072Z" fill="#1994FF"/>
              <path d="M31.8758 20.2607C28.7228 20.2607 28.2832 23.4073 28.2832 23.4073H34.9883C34.9888 23.4073 35.0288 20.2607 31.8758 20.2607Z" fill="#1994FF"/>
              <path d="M22.5 0C10.0736 0 0 10.074 0 22.5C0 34.926 10.0736 45 22.5 45C34.9264 45 45 34.926 45 22.5C45 10.074 34.9264 0 22.5 0ZM27.4056 13.2804H35.8274V15.794H27.4056V13.2804ZM23.1491 26.4641C23.1491 32.6942 16.6654 32.4893 16.6654 32.4893H10.7236H10.5491H6.04358V12.078H10.5491H10.7236H16.665C19.893 12.078 22.4411 13.86 22.4411 17.5125C22.4411 21.1654 19.3263 21.397 19.3263 21.397C23.4322 21.397 23.1491 26.4641 23.1491 26.4641ZM31.9166 29.6834C35.3095 29.6834 35.1893 27.4875 35.1893 27.4875H38.7815C38.7815 33.3152 31.7969 32.9161 31.7969 32.9161C23.4152 32.9161 23.9543 25.113 23.9543 25.113C23.9543 25.113 23.9478 17.2713 31.796 17.2713C40.0581 17.2713 38.9012 26.1308 38.9012 26.1308H28.3244C28.3244 29.9237 31.9166 29.6834 31.9166 29.6834Z" fill="#1994FF"/>
            </svg>              
          </a>
          <a href="https://dribbble.com/shots/11224555-Aeriform-Cinema-Website-Design-for-Aerial-Drone-Video-Company" target="_blank" rel="noopener noreferrer">
            <svg width="46" height="45" viewBox="0 0 46 45" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M34.8486 10.0867C36.2226 8.55542 37.0237 6.9522 37.3046 5.30649C33.4194 2.05311 28.4217 0 22.9695 0C20.1544 0 17.4698 0.5448 14.9863 1.49803C19.145 5.66455 22.6228 10.5979 25.343 15.9232C29.3512 14.4739 32.692 12.491 34.8486 10.0867Z" fill="#1994FF"/>
              <path d="M36.8064 11.8442C34.3925 14.5346 30.8044 16.7227 26.5176 18.3129C27.0504 19.4814 27.54 20.6741 28.0009 21.8822C30.2023 21.4087 32.484 21.1394 34.8109 21.1394C38.4741 21.1394 42.0327 21.761 45.3142 22.9033C45.3173 22.7529 45.3368 22.6066 45.3368 22.4551C45.3368 16.6628 43.1049 11.3947 39.4797 7.41797C38.9448 8.95917 38.0571 10.4507 36.8064 11.8442Z" fill="#1994FF"/>
              <path d="M0.939453 17.729C3.73644 18.2234 6.83529 18.5085 9.81216 18.5085C14.4269 18.5085 18.8596 17.8533 22.8103 16.7281C20.088 11.4977 16.5882 6.6839 12.4111 2.6582C6.72633 5.7156 2.34017 11.2479 0.939453 17.729Z" fill="#1994FF"/>
              <path d="M45.0869 25.6132C41.9127 24.4184 38.4191 23.7715 34.8104 23.7715C32.7875 23.7715 30.8008 23.9836 28.8848 24.3708C30.8299 30.2042 31.9555 36.5095 32.1477 42.9186C38.991 39.8229 43.9932 33.3021 45.0869 25.6132Z" fill="#1994FF"/>
              <path d="M0.425781 22.455C0.425781 29.375 3.7614 35.6573 8.71257 39.7632C9.23715 36.2734 10.8106 32.8401 13.3732 29.9667C16.4213 26.5492 20.6512 23.99 25.4414 22.5294C24.9956 21.3794 24.5087 20.2522 23.9947 19.1406C19.7145 20.4101 14.8935 21.1393 9.81142 21.1393C6.70229 21.1393 3.46673 20.8422 0.533713 20.3269C0.467241 21.0282 0.425781 21.7368 0.425781 22.455H0.425781Z" fill="#1994FF"/>
              <path d="M15.337 31.7186C12.7483 34.6214 11.3459 38.0259 11.166 41.5089C14.5969 43.6514 18.6335 44.9109 22.968 44.9109C25.2469 44.9109 27.4456 44.5642 29.5189 43.9279C29.4182 37.3766 28.3183 30.9349 26.3187 25.0059C21.9414 26.3185 18.0911 28.6303 15.337 31.7186Z" fill="#1994FF"/>
            </svg>                          
          </a>
        </div>
      </div>
    </div>
  </div>
</section>