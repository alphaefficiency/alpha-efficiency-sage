<div class="hero">
  <div class="title-conent">
    <h1>We’re looking for <br> <strong>Content Writer</strong></h1>
    <a href="{{ get_site_url() }}/schedule-a-call">Join The Future</a>
  </div>  
  <div class="bgImg">
   <div class="anm">
    <p> Explore The Opportunity</p>
    <a href="#section2">
    <svg width="29" height="12" viewBox="0 0 29 12" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M1 1L14.5 11L28 1" stroke="#878787"/>
    </svg>    
    </a>
   </div>
  </div>
</div>