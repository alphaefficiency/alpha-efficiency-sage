<section class="blog-section">
  <div class="bg-eighth">
    <svg width="1920" height="263" viewBox="0 0 1920 263" fill="none" xmlns="http://www.w3.org/2000/svg">
      <g filter="url(#filter0_d)">
      <mask id="mask0" mask-type="alpha" maskUnits="userSpaceOnUse" x="0" y="0" width="1920" height="255">
      <rect x="1920" y="255" width="1920" height="255" transform="rotate(-180 1920 255)" fill="#F5F5F5"/>
      </mask>
      <g mask="url(#mask0)">
      <path d="M2994.37 185.574C2994.37 185.574 2129.76 172.894 1382.09 112.788C634.424 52.6822 569.123 205.242 -175.428 145.387C-923.095 85.2815 -1057.94 231.042 -1057.94 231.042" stroke="#FFE500" stroke-width="2"/>
      <path d="M2988.04 213.993C2988.04 213.993 2171.94 95.3176 1424.27 35.2119C676.605 -24.8938 557.568 232.246 -186.983 172.391C-934.65 112.285 -1056.12 252.23 -1056.12 252.23" stroke="#1994FF" stroke-width="2"/>
      <path d="M2989.3 237.762C2989.3 237.762 2164.43 134.071 1416.76 73.9652C669.092 13.8595 550.049 249.796 -194.502 189.94C-942.169 129.835 -1102 391.14 -1102 391.14" stroke="#FFB000" stroke-width="2"/>
      <path d="M2991.83 116.852C2991.83 116.852 2106.64 226.896 1358.97 166.791C611.306 106.685 538.493 276.797 -206.058 216.941C-953.725 156.836 -1053.74 283.909 -1053.74 283.909" stroke="#ED462F" stroke-width="2"/>
      </g>
      </g>
      <defs>
      <filter id="filter0_d" x="-4" y="25.1139" width="1928" height="237.886" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
      <feFlood flood-opacity="0" result="BackgroundImageFix"/>
      <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/>
      <feOffset dy="4"/>
      <feGaussianBlur stdDeviation="2"/>
      <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0"/>
      <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow"/>
      <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape"/>
      </filter>
      </defs>
    </svg>      
  </div>
  <div class="container">
    <div class="top">
      <h1>Our Recent Blog Posts</h1>
      <hr>
    </div>

    <div class="bottom">
      {{-- POST CODE GOES HERE --}}
      @php
        $recent_posts = wp_get_recent_posts(array(
              'numberposts' => 3,
              'post_status' => 'publish'
          )); 
      @endphp

      @foreach($recent_posts as $post)
      @php
      $attachments = get_posts(array(
        'post_type' => 'attachment', 
        'post_mime_type'=>'image', 
        'posts_per_page' => 0, 
        'post_parent' => $post->ID, 
        'order'=>'ASC'
      ));   
      @endphp
        <div class="post section-{{$index++}} dot-current">
          <div class="image">
            @if (has_post_thumbnail($post['ID']))
              <?=get_the_post_thumbnail($post['ID'], 'full')?>
            @elseif (!$attachments)
              <img class="post-thumbnail" src="@asset('images/blog/post-image-holder-min.png')" alt="Post image">
            @else
              <img class="post-thumbnail" src="<?=$attachments[0]->guid?>" alt="Post image"/>
            @endif
          </div>
          <div class="text">
            <div class="date">
              <?= get_the_date('d. M Y', $post['ID'])?>
            </div>
            <div class="title">
              {{ Posts::getExcerptForBlogCarouselTitle($post['post_title']) }}
              <hr>
            </div>

            <div class="text-content">
              {{ Posts::getExcerptForPostContent($post["link"] , $post["post_content"]) }}
            </div>
            <a href="{{ get_permalink($post->ID) }}" class="read-more">Read More</a>
          </div>
        </div>
      @endforeach


      <div id="posts-carousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
          @foreach($recent_posts as $post) 
          <div class="post-carousel carousel-item">
            <div class="image">
              @if (has_post_thumbnail($post['ID']))
              <?=get_the_post_thumbnail($post['ID'], 'full')?>
              @elseif (!$attachments)
                <img class="post-thumbnail" src="@asset('images/blog/post-image-holder-min.png')" alt="Post image">
              @else
                <img class="post-thumbnail" src="<?=$attachments[0]->guid?>" alt="Post image"/>
              @endif
            </div>
            <div class="text">
              <div class="date">
                <?= get_the_date('d. M Y', $post['ID'])?>
              </div>
              <div class="title">
                {{ Posts::getExcerptForBlogCarouselTitle($post['post_title']) }}
                <hr>
              </div>
  
              <div class="text-content">
                {{ Posts::getExcerptForPostContent($post["link"] , $post["post_content"]) }}
              </div>
              <a href="{{ get_permalink($post->ID) }}" class="read-more">Read More</a>
            </div>
          </div>
        @endforeach
        </div>
      </div>
    </div>
  </div>
</section>