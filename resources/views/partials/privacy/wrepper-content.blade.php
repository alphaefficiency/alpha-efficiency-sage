<div class="wrepper">
  <div class="text-content">
    <p> <b>Your Privacy Your privacy is important to us.</b> <br>
       To better protect your privacy we provide this notice explaining our online information practices and the choices you can make about the way your information is collected and used. To make this notice easy to find, we make it available on our homepage and at every point where personally identifiable information may be requested.</p>
      <p> <b> Google Adsense and the DoubleClick DART Cookie </b> <br>
        Google, as a third party advertisement vendor, uses cookies to serve ads on this site. The use of DART cookies by Google enables them to serve adverts to visitors that are based on their visits to this website as well as other sites on the internet. <br><br>
        To opt out of the DART cookies you may visit the Google ad and content network privacy policy at the following url <a href="http://www.google.com/privacy_ads.html" target="_blanc">http://www.google.com/ privacy_ads.html</a> Tracking of users through the DART cookie mechanisms are subject to Google’s own privacy policies. <br> <br>
        Other Third Party ad servers or ad networks may also use cookies to track users activities on this website to measure advertisement effectiveness and other reasons that will be provided in their own privacy policies, Increasing Workplace Productivity has no access or control over these cookies that may be used by third party advertisers.</p>
        <p><b>Collection of Personal Information</b><br>
        When visiting Increasing Workplace Productivity, the IP address used to access the site will be logged along with the dates and times of access. This information is purely used to analyze trends, administer the site, track users movement and gather broad demographic information for internal use. Most importantly, any recorded IP addresses are not linked to personally identifiable information.</p>
        <p><b>Links to third party Websites</b><br>
         We have included links on this site for your use and reference. We are not responsible for the privacy policies on these websites. You should be aware that the privacy policies of these sites may differ from our own.</p>
        <p><b>Changes to this Privacy Statement</b><br>
         The contents of this statement may be altered at any time, at our discretion. <br> <br>
          If you have any questions regarding the privacy policy of Increasing Workplace Productivity then you may contact us at <i> bojan@alphaefficiency.com</i> </p> 
    </p>
  </div>
</div>