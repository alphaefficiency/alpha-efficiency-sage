<section class="third-section">
  <div class="container">
   <div class="third-part">
     <div class="info">
       <h2>Locations</h2>
       <p>
        Finding an agency to fulfill your web development, design, SEO, and PPC goals
        can be challenging. Our mission is to connect our clients with our senior-level
        experts to come up with the most innovative and profitable solutions.
       </p>
       <p>
        We'd love to offer you the convenience of setting up your digital transformation
        from the comfort of your home. Schedule a call with us to start your Alpha Efficiency journey,
        or find us at the nearest location in the Chicago area.
       </p>
       <a href="https://alphaefficiency.com/about">See More About Us</a>
     </div>
     <div class="third-img">
      <img src="@asset('images/locations/map.png')" alt="map" class="img-fluid/">
     </div>
   </div>
  </div>
</section>
