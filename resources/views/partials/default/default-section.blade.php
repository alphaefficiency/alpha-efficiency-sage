<main>
  <div class="hearo">
    <h1>Default Page</h1>
  </div>
  <div class="text">
    @php
        wp_reset_query(); // necessary to reset query
          while ( have_posts() ) : the_post();
            the_content();
          endwhile; // End of the loop.
        @endphp
  </div>
  <div class="btn">
    <a href="#">Get Started</a>
  </div>
  <div class="Img"></div>
</main>