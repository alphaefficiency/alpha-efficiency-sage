<section class="productivity">
    <div class="img-frame">
        <img src="@asset('images/about/iphone1.svg')" alt="Commerc" class="img-fluid img-iphone"/>
        <img src="@asset('images/about/iphone-500.svg')" alt="Commerc" class="img-fluid d-none"/>
    </div>
    <div class="decade">
        {{-- <div class="decor">
            <svg width="74" height="74" viewBox="0 0 74 74" fill="none" xmlns="http://www.w3.org/2000/svg">
                <circle cx="37" cy="37" r="37" fill="white"/>
                <rect x="19" y="19" width="16.4286" height="16.4286" rx="8.21429" fill="#EE4631"/>
                <rect x="37.5723" y="19" width="16.4286" height="16.4286" rx="8.21429" fill="#FFE600"/>
                <rect x="19" y="37.5714" width="16.4286" height="16.4286" rx="8.21429" fill="#FFB000"/>
                <rect x="37.5723" y="37.5714" width="16.4286" height="16.4286" rx="8.21429" fill="#1994FF"/>
            </svg>                
        </div> --}}
        <h4 class="title">Decade Of Improved Productivity</h4>
        <div class="line"></div>
        <p class="first">Alpha Efficiency holds roots in productivity, technology and creativity. We started off as a Magazine dedicated to challenging cliche status quo in Productivity, but we transcended into challenging status quo in communication, marketing and technology.</p>
        <a href="#" class="see-more">See More</a>
        <p class="hide">With over a decade in business, we’ve empowered countless dreams and transformed them into tangible realities for hundreds of our clients. It’s time for you to join the tribe of digitally empowered businesses.</p> 
        <p class="hide">Our expertise is technology and digital, but we never forget that there is a human being on the other side of the screen. People are now more informed than ever, and speaking to their logic doesn’t cut it anymore. Our advertising here ignites the emotion, connects to the core of the problem through our powerful creatives, design and intuitive understanding human psychology.</p>
        <a href="{{ get_site_url() }}/portfolio">
            See Our Work
            <svg width="30" height="1" viewBox="0 0 30 1" fill="none" xmlns="http://www.w3.org/2000/svg">
                <rect width="30" height="1" fill="#007BE7"/>
            </svg>                
        </a>
    </div>
</section>