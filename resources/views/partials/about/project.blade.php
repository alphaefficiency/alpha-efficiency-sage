<section class="project">
    <div class="top-div">
        <p class="text-big">Go beyond boring old corporate that creates a lot of overhead, yet no results. Go with <strong>Alpha Efficiency.</strong></p>
        <div class="line-ver"></div>
        <p class="text-sm">We started doing all of this so we can escape the 9 to 5 culture that is focused on punching in and punching out. If you are tired of your employees that come to the office, punch in their hours without you even noticing the impact that they’ve left, than you’ve came to the right place. Alpha Efficiency isn’t about the hours, we are about the dollars, and results that keep yours and our business going. Upgrade your marketing, elevate your team. Start working with those that make the difference.</p>
    </div>
</section>
<section class="about-form">
    @component('components.forms.project', ['title' => 'Have A Project to Complete?']) @endcomponent
</section>