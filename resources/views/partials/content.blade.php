<article class="single-post">
  <div class="post-content-wrapper container">

    <div class="post-title-wrapper">

      <a class="post-image-link" href="{{ $post->link }}">
        @if (has_post_thumbnail($post->ID))
          <?=get_the_post_thumbnail($post->ID)?>
        @elseif (!$attachments)
          <img class="post-thumbnail" src="@asset('images/blog/post-image-holder-min.png')">
        @else
          <img class="post-thumbnail" src="<?=$attachments[0]->guid?>">
        @endif

      </a>
    </div>

    <div class="entry-content">
      <div class="time-title">
        <div class="time">
          <?= get_the_date('F j, Y', $post->ID)?>
          </div>
          <h2>
            <a class="post-title" href="{{$post->link}}">
              {{-- {{ Posts::getExcerptForPostTitle($post->post_title) }} --}}
            </a>
          </h2>
          <hr>
      </div>

      <div class="post-desc-div">
        <p class="post-description">
          @php echo Posts::getExcerptForPostContent($post->link , $post->post_content) @endphp
        </p>
        {{-- Read More button --}}
        <a class="read-more-btn" href="{{$post->link}}">Read More</a>
      </div>
    </div>

  </div>
  </article>