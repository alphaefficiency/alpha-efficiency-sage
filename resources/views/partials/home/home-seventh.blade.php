<section class="seventh" id="contact">
    <div class="seventh-frame">
        <div class="delivers">
            <div class="delivers-title">
                <h2>Proven Process <br/>That <b>Delivers</b>.</h2>
            </div>
            <div class="delivers-txt">
                <p>Alpha Efficiency takes a <strong>process-driven approach</strong> to finding solutions to the complex challenges businesses face when navigating the future. Our senior-level experts work in <strong>cross-functional teams of strategists</strong>, designers and developers to deliver fresh thinking on everything from apps to automation.</p>
            </div>
            <div class="contact-arrow">
                <a href="{{ get_site_url() }}/contact">Contact Us</a><span></span>
            </div>
        </div>
        <div class="about-form">
            @component('components.forms.project', ['title' => 'Have A Project</br> To Complete?']) @endcomponent
        </div>
    </div>
</section>