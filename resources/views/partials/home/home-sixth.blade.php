<section class="sixth" id="portfolio">
  <div class="container-fluid">
      <div class="projects">
          <div class="img-wrap">
            <div class="d-flex justify-content-between">
              <a href="https://www.surge-strength.com" target="_blank">
                <img src="@asset('images/home/projects-coffee.png')" alt="Projects" class="img-fluid"/>
              </a>
              <a href="https://www.breachlock.com/" target="_blank">
                <img src="@asset('images/home/projects-breachlock.png')" alt="Projects" class="img-fluid"/>
              </a>
            </div>
            
            <a href="https://aeriformcinema.com" target="_blank">
              <img src="@asset('images/home/projects-aeriform.png')" alt="Projects" class="img-fluid"/>
            </a>
          </div>
          <div class="latest-projects">
              <h2>Our Latest Projects</h2>
              <div class="line"></div>
              <p>Let us introduce you to our latest projects that transformed perception of our client brands.</p>
              <a href="{{ get_site_url() }}/portfolio">Explore&nbsp;&nbsp;&nbsp;<svg width="30" height="1" viewBox="0 0 30 1" fill="none" xmlns="http://www.w3.org/2000/svg">
                <rect width="30" height="1" fill="#007BE7" />
              </svg></a>
          </div>
      </div>
  </div>
</section>