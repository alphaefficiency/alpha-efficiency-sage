<section class="fourth" id="explore">
  <div class="container-fluid">
      <div class="explore">
          <h2>Explore Our Magazine</h2>
          <div class="line"></div>
          <p>Follow our Magazine, get the latest updates on ways technology improves your business performance. Follow up with our software developement updates, intricacies of search engine optimization, and our work hacks.</p>
          <p>All of our knowledge stored, organized and conveiently shared so you can benefit from our expertise.</p>
          <a href="{{ get_site_url() }}/blog">Explore&nbsp;&nbsp;&nbsp;<svg width="30" height="1" viewBox="0 0 30 1" fill="none" xmlns="http://www.w3.org/2000/svg">
            <rect width="30" height="1" fill="#007BE7" />
          </svg></a>
      </div>
      <div class="explore-img">
          <a href="{{ get_site_url() }}/category/app" class="mb-5 app">
            <img src="@asset('images/home/fourth-section-icons/app.svg')" width="82" height="107" alt="Explore" class="img-fluid"/>
            <span>Applications</span>
          </a>
          <a href="{{ get_site_url() }}/category/books" class="mb-5 books">
            <img src="@asset('images/home/fourth-section-icons/books.svg')" width="82" height="107" alt="Explore" class="img-fluid"/>
            <span>Books</span>
          </a>
          <a href="{{ get_site_url() }}/category/email-marketing" class="mb-5 email">
            <img src="@asset('images/home/fourth-section-icons/email-marketing.svg')" width="100" height="107" alt="Explore" class="img-fluid"/>
            <span>Email <br/>Marketing</span>
          </a>
          <a href="{{ get_site_url() }}/category/ppc" class="mb-5 ppc">
            <img src="@asset('images/home/fourth-section-icons/ppc.svg')" width="82" height="107" alt="Explore" class="img-fluid"/>
            <span>PPC</span>
          </a>
          <a href="{{ get_site_url() }}/category/seo" class="mb-5 seo">
            <img src="@asset('images/home/fourth-section-icons/seo.svg')" width="82" height="107" alt="Explore" class="img-fluid"/>
            <span>SEO</span>
          </a>
          <a href="{{ get_site_url() }}/category/time-management" class="mb-5 time">
            <img src="@asset('images/home/fourth-section-icons/time-managment.svg')" width="109" height="107" alt="Explore" class="img-fluid"/>
            <span>Time Managment</span>
          </a>
          <a href="{{ get_site_url() }}/category/tools" class="mb-5 tools">
            <img src="@asset('images/home/fourth-section-icons/tools.svg')" width="82" height="107" alt="Explore" class="img-fluid"/>
            <span>Tools</span>
          </a>
          <a href="{{ get_site_url() }}/category/web-design-and-development" class="mb-5 web">
            <img src="@asset('images/home/fourth-section-icons/design-development.svg')" width="89" height="122" alt="Explore" class="img-fluid"/>
            <span>Web Design & <br/> Development</span>
          </a>
          <a href="{{ get_site_url() }}/category/workflows" class="mb-5 workflow">
            <img src="@asset('images/home/fourth-section-icons/workflow.svg')" width="82" height="107" alt="Explore" class="img-fluid"/>
            <span>Workflow</span>
          </a>
      </div>
  </div>
</section>