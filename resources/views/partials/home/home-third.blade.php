<section class="third" id="intelligence">
    <div class="container-fluid">
        <div class="intelligence">
          <h2>Business Intelligence</h2>
          <div class="line"></div>
          <p>Alpha Efficiency engages in the research & development of software & digital solutions for business purposes. We serve as an independent resource of analysis, code, business intelligence to ensure that our business partners get the best possible data and software tools that provide sustainable growth.</p>
          <a href="{{ get_site_url() }}/about">Learn More&nbsp;&nbsp;&nbsp;<svg width="30" height="1" viewBox="0 0 30 1" fill="none" xmlns="http://www.w3.org/2000/svg">
              <rect width="30" height="1" fill="#007BE7" />
            </svg></a>
        </div>
        <div class="featured-on">
          <div class="featured-box">
              <p>Featured On</p>
              <div class="line"></div>
              <div class="frame">
                <div class="frame">
                    <a>
                        <img src="@asset('images/home/lifehacker.svg')" width="199" height="49" alt="Lifehacker" class="img-fluid"/>
                    </a> 
                    <a href="https://www.macsparky.com/blog/2013/12/home-screens-bojan-dordevic" target="_blank">
                        <img src="@asset('images/home/macsparky.svg')" width="105" height="113"alt="Lifehacker" class="img-fluid"/>
                     </a>   
                    <a href="https://productivityist.com/5-best-ways-to-use-evernote/" target="_blank">
                        <img src="@asset('images/home/productivitist.svg')" width="167" height="121" alt="Lifehacker" class="img-fluid"/>
                    </a>   
                    <a href="https://www.smartpassiveincome.com/blog/10-posts-im-glad-i-didnt-miss/" target="_blank">
                        <img src="@asset('images/home/spi.svg')" width="161" height="166" alt="Lifehacker" class="img-fluid"/>
                    </a>                      
                  </div>                       
              </div>
          </div>
          <div class="eye">
              <svg width="74" height="74" viewBox="0 0 74 74" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <circle cx="37" cy="37" r="37" fill="#1994FF"/>
                  <path d="M39.8913 40.7438C39.8913 40.7438 32.6237 52.5578 27.1485 50.3737C21.6733 48.1896 27.6887 37.3436 27.6887 37.3436C27.6887 37.3436 30.6104 31.4614 35.0544 31.3621C39.4984 31.2628 39.8913 40.7438 39.8913 40.7438Z" fill="white"/>
                  <path d="M19.6848 42.4072L13.3994 53.4022H25.6265C25.6265 53.3774 17.8189 51.5904 19.6848 42.4072Z" fill="white"/>
                  <path d="M32.1318 53.4017C32.1318 53.4017 38.8592 51.441 42.1984 46.7998C42.1984 46.7998 42.5666 52.2601 47.6244 53.3769L32.1318 53.4017Z" fill="white"/>
                  <path d="M49.4181 34.9863L60.0002 53.2782L50.1301 53.3775C50.1301 53.3775 52.1679 52.9059 53.8375 50.97C53.8375 50.97 54.0094 50.7218 53.5674 50.8211C53.1255 50.9203 48.7306 51.7642 47.2575 47.2719L45.416 41.5635L49.4181 34.9863Z" fill="white"/>
                  <path d="M43.4019 34.5886C43.4019 34.5886 41.6587 28.6567 35.5697 28.6567C29.1615 28.6567 24.9385 33.4965 24.9385 33.4965L36.7482 12.5986L46.2746 29.6495L43.4019 34.5886Z" fill="white"/>
              </svg>                               
          </div>
        </div>
    </div>
  </section>