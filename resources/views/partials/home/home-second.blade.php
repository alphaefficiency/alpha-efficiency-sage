<section class="second" id="grow">
    <div class="second-container">
      <div class="decor">
        <svg width="138" height="71" viewBox="0 0 138 71" fill="none" xmlns="http://www.w3.org/2000/svg">
          <rect x="50" y="15" width="18.3061" height="18.3061" rx="9.15306" fill="#EE4631"/>
          <rect x="70.6938" y="15" width="18.3061" height="18.3061" rx="9.15306" fill="#FFE600"/>
          <rect x="50" y="35.6938" width="18.3061" height="18.3061" rx="9.15306" fill="#FFB000"/>
          <rect x="70.6938" y="35.6938" width="18.3061" height="18.3061" rx="9.15306" fill="#1994FF"/>
          <line x1="4.37114e-08" y1="34.5" x2="138" y2="34.5" stroke="#DADADA"/>
          <line x1="69.5" y1="-2.18557e-08" x2="69.5" y2="71" stroke="#DADADA"/>
        </svg>          
      </div>
      <div class="second-box upper">
        <iframe
          width="652"
          height="358"
          src="https://www.youtube.com/embed/REgdFeNIXc4"
          srcdoc="<style>*{padding:0;margin:0;overflow:hidden}html,body{height:100%}img,span{position:absolute;width:100%;top:0;bottom:0;margin:auto}span{height:1.5em;text-align:center;font:48px/1.5 sans-serif;color:white;text-shadow:0 0 0.5em black}</style><a href=https://www.youtube.com/embed/REgdFeNIXc4?autoplay=1><img src=@asset('images/home/testvideo.jpg') alt='Alpha Efficiency Promo Video' data-no-image-dimensions style='max-width: 100%; height: auto';></a>"
          frameborder="0"
          allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
          allowfullscreen
          scrolling="no"
        ></iframe>
        <div class="second-text">
          <h2>Complete</h2>
          <div class="line"></div>
          <p>We lead with services that enable communication & server operations in order to gain a competitive advantage for our business partners.</p>
          <a href="{{ get_site_url() }}/schedule-a-call">Grow Your Business&nbsp;&nbsp;&nbsp;<svg width="30" height="1" viewBox="0 0 30 1" fill="none" xmlns="http://www.w3.org/2000/svg">
            <rect width="30" height="1" fill="#007BE7" />
          </svg></a>
        </div>
      </div>
      <div class="second-box lower">
        <div class="second-text">
          <h2>The Mission</h2>
          <div class="line"></div>
          <p>We help forward-thinking companies finalize their digital transformation by building their infrastructure, perfecting their business performance, digital communication, and software environment.</p>
          <a href="{{ get_site_url() }}/schedule-a-call">Get Inspired&nbsp;&nbsp;&nbsp;<svg width="30" height="1" viewBox="0 0 30 1" fill="none" xmlns="http://www.w3.org/2000/svg">
            <rect width="30" height="1" fill="#007BE7" />
          </svg></a>
        </div>
        <div class="img-frame">
          <img src="@asset('images/home/rocket.jpg')" width="637" height="358" alt="Mission" class="img-fluid"/>
        </div>
      </div>
    </div>
</section>