<section class="get-started">
    <div class="started-box" style="max-width: 880px;">
        <div class="p-box" style="display: flex; justify-content: space-between;">
            <p>Your paid search performance is about to take off. <strong>6 million dollars on ad spend experience tells you you’re on the right page.</strong> Alpha Efficiency’s team has been successfully executing lead generation and search marketing campaigns for the past decade, and now it’s time for your business to experience what ROI positive means.<span class="hidden-service" style="color:#1994ff; margin-left:5px;">Read More</span></p>
            <p class="hidden-content-service"><strong>Work with Talent</strong> ranging from top performing user experience designers to high performance web analysts that will get the returns you deserve. Empower your business with next level search technology that cuts through the noise and reduces your lead cost in return improving your bottom line.</p>
        </div>
        <a href="{{ get_site_url() }}/schedule-a-call" class="get-started-btn">Get Started</a>
    </div>
</section>