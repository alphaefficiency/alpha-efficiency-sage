<section class="solution-section">
  <div class="solution-part">
    <div class="solution-text">
      <h1>The Solution</h1>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
        magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
        consequat.
        Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariature xcepteur
        sint occaecat.
      </p>
    </div>
    <div class="solution-img">
      <img class="img1" src="@asset('images/case-study-surge/solution.png')" alt="Alpha Delivered" />
      <img class="img2" src="@asset('images/case-study-surge/solution-mobile.png')" alt="Alpha Delivered" />
    </div>
  </div>
</section>
