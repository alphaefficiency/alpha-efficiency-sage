<section class="offer">
    <div class="offer-frame">
        <div class="offer-text">
            <h2>What We Offer</h2>
            <div class="line"></div>
            <p class="first"><strong>Alpha Efficiency</strong> holds roots in productivity, technology and creativity.</p> 
            <p>We started off as a Magazine dedicated to challenging cliche status quo in Productivity, but we transcended into challenging status quo in communication, marketing and technology.</p>
            <a href="#">
                See More About Us
                <svg width="50" height="1" viewBox="0 0 50 1" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <rect x="0.362793" width="49.5085" height="1" fill="#007BE7"/>
                </svg>                    
            </a>
        </div>
        <div class="offer-img">
            <img src="@asset('images/hiring/main.png')" alt="Cover Photo" class="img-fluid"/>
        </div>
    </div>
</section>