<section class="benefits">
    <div class="title-wrap">
        <h2 class="title">Benefits</h2>
        <div class="line-t"></div>
    </div>
    <div class="benefits-wrap">
        <div class="benefit">
            <img src="@asset('images/hiring/1.png')" alt="Benefit" class="img-fluid"/>
            <div class="text-box">
                <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg" class="dot">
                    <rect width="30" height="29.4545" rx="14.7273" fill="#EE4631"/>
                </svg>
                <p>Remote Work Available</p>                    
            </div>
        </div>
        <div class="benefit">
            <img src="@asset('images/hiring/2.png')" alt="Benefit" class="img-fluid"/>
            <div class="text-box">
                <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg" class="dot">
                    <rect width="30" height="29.4545" rx="14.7273" fill="#FFE600"/>
                </svg>
                <p>Opportunity For growth</p>                    
            </div>
        </div>
        <div class="benefit">
            <img src="@asset('images/hiring/3.png')" alt="Benefit" class="img-fluid"/>
            <div class="text-box">
                <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg" class="dot">
                    <rect width="30" height="29.4545" rx="14.7273" fill="#FFB000"/>
                </svg>
                <p class='text-box-sm'>Work Life Balance</p>                    
            </div>
        </div>
        <div class="benefit">
            <img src="@asset('images/hiring/4.png')" alt="Benefit" class="img-fluid"/>
            <div class="text-box">
                <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg" class="dot">
                    <rect width="30" height="29.4545" rx="14.7273" fill="#1994FF"/>
                </svg>
                <p>Flexible Working Hours</p>                    
            </div>
        </div>
    </div>
</section>