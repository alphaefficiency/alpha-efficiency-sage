<div class="problem-section">
  <div class="problem-part">
    <div class="problem-img">
      <img src="@asset('images/case-study-surge/problem.png')" alt="Alpha Delivered" />
    </div>
    <div class="problem-text">
      <h1>The Problem</h1>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing
        elit, sed do eiusmod tempor incididunt ut labore et
        dolore magna aliqua. Ut enim ad minim veniam, qus.
      </p>
      <ul>
        <li>Lorem ipsum dolor sit</li>
        <li>Lorem ipsum dolor sit</li>
        <li>Lorem ipsum dolor sit</li>
        <li>Lorem ipsum dolor sit</li>
      </ul>
    </div>
  </div>
</div>
