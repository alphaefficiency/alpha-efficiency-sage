<div class="hero">
  <div class="container">
    <h1>We're <span>Hiring</span></h1>
  </div>
</div>
<div class="offer">
  <div class="container">
    <div class="text-wrap">
      <h2 class="title">Respect for People</h2>
      <div class="line"></div>
      <p> We are committed to ensuring that all personnel is respected, included, and valued for their diverse backgrounds, experiences, skills, and contributions to our mission and culture.</p>
      {{-- <p>We started off as a Magazine dedicated to challenging cliche status quo in Productivity, but we transcended into challenging status quo in communication, marketing and technology.</p> --}}
      <a href="{{ get_site_url() }}/about">See More About Us <svg width="50" height="1" viewBox="0 0 50 1" fill="none" xmlns="http://www.w3.org/2000/svg">
        <rect x="0.363281" width="49.5085" height="1" fill="#007BE7"/>
        </svg>
        </a>
    </div>
    <div class="img-wrap">
      <img src="@asset('images/careers/offer.png')" alt="What We Offer" class="img-fluid"/>
    </div>
  </div>
</div>