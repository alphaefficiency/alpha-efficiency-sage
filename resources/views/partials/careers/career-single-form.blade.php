<div class="from-content">
  <div class="title"> 
    <h2>We are committed to ensuring that all personnel is respected, included, and valued for their diverse backgrounds, experiences, skills and contributions.</h2>
    <div class="line"></div>
  </div>
  <div class="form">
    {{-- @component('components.forms.upload-cv') @endcomponent --}}
    @if (is_page('content-writer'))
      <?= do_shortcode('[contact-form-7 id="16015" title="Content Writer CV"]') ?>  
    @else
      <?= do_shortcode('[contact-form-7 id="14289" title="Upload CV"]') ?>
    @endif
  </div>
</div>