<div class="benefits">
  <h2 class="title">Benefits</h2>
  <div class="line"></div>
  <div class="container">
    <div class="item">
      <img src="@asset('images/careers/remote.png')" alt="Remote Work Available"/>
      <div class="desc">
        <div class="dot" style="background: #EE4631"></div>
        <p>Remote Work <br/>Available</p>
      </div>
    </div>
    <div class="item">
      <img src="@asset('images/careers/growth.png')" alt="Opportunity For
      growth"/>
      <div class="desc">
        <div class="dot" style="background: #FFE600"></div>
        <p>Opportunity For <br/>
          Growth</p>
      </div>
    </div>
    <div class="item">
      <img src="@asset('images/careers/balance.png')" alt="Work Life
      Balance"/>
      <div class="desc">
        <div class="dot" style="background: #FFB000"></div>
        <p>Work Life <br/>
          Balance</p>
      </div>
    </div>
    <div class="item">
      <img src="@asset('images/careers/flex.png')" alt="Flexible Working
      Hours"/>
      <div class="desc">
        <div class="dot" style="background: #1994FF"></div>
        <p>Flexible Working <br/>
          Hours</p>
      </div>
    </div>
  </div>
</div>