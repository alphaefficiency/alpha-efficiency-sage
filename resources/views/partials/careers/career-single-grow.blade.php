<section class="second" id="section-1">
  <div class="container-fluid">
    <div class="content-holder">
      <div class="circle-dots">
        <svg width="74" height="74" viewBox="0 0 74 74" fill="none" xmlns="http://www.w3.org/2000/svg">
          <circle cx="37" cy="37" r="37" fill="white"/>
          <rect x="19" y="19" width="16.4286" height="16.4286" rx="8.21429" fill="#EE4631"/>
          <rect x="37.5714" y="19" width="16.4286" height="16.4286" rx="8.21429" fill="#FFE600"/>
          <rect x="19" y="37.5715" width="16.4286" height="16.4286" rx="8.21429" fill="#FFB000"/>
          <rect x="37.5714" y="37.5715" width="16.4286" height="16.4286" rx="8.21429" fill="#1994FF"/>
        </svg>        
        </div>
      <div class="upper-content">
        <div class="left">

        </div>
        <div class="right">
          <h2>Grow</h2>
          <hr>
          <p>We’re in business since 2010, and we’ve been delivering profitable projects to our clients ever since. We specialize in Web development and online marketing.</p>
          <a href="{{ get_site_url() }}/schedule-a-call">Grow Your Business</a>
        </div>
      </div>
      <div class="bottom-content">
        <div class="left">
          <h2>Quality</h2>
          <hr>
          <p>We make our clients lives easier and less stressful. Our service based business is all about customer satisfaction.</p>
          <a href="{{ get_site_url() }}/schedule-a-call">Get Inspired</a>
        </div>
        <div class="right">

        </div>
      </div>
    </div>
  </div>
</section>