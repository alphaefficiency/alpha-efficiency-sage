<head>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-PMBVTDM');</script>
  <!-- End Google Tag Manager -->
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="icon" href="@asset('images/shared/favicon.svg')">
  {{-- <link rel="preload" href="@asset('fonts/Lora/Lora-VariableFont_wght.ttf')" as="font" type="font/ttf" crossorigin>
  <link rel="preload" href="@asset('fonts/Mulish/Mulish-VariableFont_wght.ttf')" as="font" type="font/tff" crossorigin> --}}
  @php wp_head() @endphp
</head>
 