<section class="get-started">
    <div class="started-box" style='max-width:966px;'>
        <p>Email marketing is the hidden gem of online advertising, as it is a go-to strategy to getting your existing prospects and clients to come back to your sales ecosystem. Alpha Efficiency has a successful track record of creating: <span class="hidden-service" style="color:#1994ff; margin-left:5px;">Read More</span></p>
        <div class="hidden-content-service">
            <ul>
                <li>Automated CRM integrated campaigns</li>
                <li>Third Party Deployments</li>
                <li>Subject Lines That Are Forcing Your Customers To Click </li>
                <li>Creating powerful reports that provide deeper insight into your audience</li>
            </ul>
            <p>Integrate email marketing into your campaigns and experience how most cost effective online channel can improve your business and profitability.</p>
        </div>
        <a href="{{ get_site_url() }}/schedule-a-call" class="get-started-btn">Get Started</a>
    </div>
</section>