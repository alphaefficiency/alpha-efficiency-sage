<div class="wrepper" id="section2">
  <div class="left">
    <h2>Apply now and join the <br> <strong> Alpha Efficiency</strong> Crew.</h2>
    <div class="line"></div>
  </div>
  <div class="right">
    <div class="line"></div>
    <div class="ul">    
     <div class="svg">
    <h2>If you...</h2>
        <svg id="up" width="22" height="13" viewBox="0 0 22 13" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M21 12L11 2L1 12" stroke="#1994FF" stroke-width="2"/>
        </svg>          
        <svg id="down" width="22" height="13" viewBox="0 0 22 13" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M21 1L11 11L1 0.999998" stroke="#1994FF" stroke-width="2"/>
        </svg>          
      </div>
      <ul id="ul">
       <li>Have more than 3 years of relevant experience</li>
       <li>Have proven track record of successfully creating PSDs for responsive Websites</li>
       <li>Can carry an entire project from the beginning to the end on your own while keeping track of the details</li> 
       <li>Are capable of producing quality creative solutions in short periods of time</li> 
       <li>Have a passion for UX, minimal and modern design</li> 
       <li>Constantly invest in your own knowledge and you are determined to become the best at what you do </li>
       <li>Speak GREAT English and have amazing communication skills</li> 
       <li>Really care about the quality and efficiency of your work</li> 
       <li>Apply common sense to your design, and understand basic needs of marketing departments.</li>
      </ul>
   </div>
   <div class="line" id="line"></div>
   <div class="line"></div>
   <div class="if">
    <div class="svg">
      <h2>You get...</h2>
          <svg id="up2" width="22" height="13" viewBox="0 0 22 13" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M21 12L11 2L1 12" stroke="#1994FF" stroke-width="2"/>
          </svg>          
          <svg id="down2" width="22" height="13" viewBox="0 0 22 13" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M21 1L11 11L1 0.999998" stroke="#1994FF" stroke-width="2"/>
          </svg>          
        </div>
      <ul id="ul2">
       <li>Competitive salary and performance bonuses upon successful project completion </li>
       <li>To Work among the marketing & development team that is determined to win</li>
       <li>To Solve Design and Creative problems in a time efficient manner</li>
       <li>To work in conveniently located Belgrade downtown office</li> 
    </ul>
  </div>
  <div class="line"></div>
  </div>
</div>