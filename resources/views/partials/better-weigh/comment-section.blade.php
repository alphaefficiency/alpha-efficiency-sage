<section class="comment-section">
  <div class="container-fluid">
    <div class="img-line">
      <img class="img1" src="@asset('images/case-study-surge/comment-line.svg')" alt="Alpha Delivered" />
      <img class="img2" src="@asset('images/case-study-surge/comment-mobile.svg')" alt="Alpha Delivered" />
    </div>
    <div class="comment-part">
      <div class="comment-item">
        <div class="img1">
          <img src="@asset('images/case-study-surge/person-img.png')" alt="Alpha Delivered" />
        </div>
        <div class="position">
          <p><b>John Smith</b>
            <span>President, CEO</span>
          </p>
        </div>
      </div>
      <div class="comment-text">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
        dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex
        ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat
        nulla pariature xcepteur sint occaecat. Ut enim ad minim veniam, quis nostru.
      </div>
    </div>
  </div>
</section>
