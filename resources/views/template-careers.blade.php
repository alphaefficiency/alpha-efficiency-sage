{{--
  Template Name: Careers
--}}

@extends('layouts.app')

@section('content')
  @include('partials.careers.hero-section')
  @include('partials.careers.second-section')
  @include('partials.careers.third-section')
@endsection