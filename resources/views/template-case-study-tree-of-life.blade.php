{{--
  Template Name: Case Study Tree Of Life
--}}

@extends('layouts.app')

@section('content')

@include('partials.tree-of-life.hero-study')
@include('partials.tree-of-life.img-surge')
@include('partials.tree-of-life.usability-section')
@include('partials.tree-of-life.problem-section')
@include('partials.tree-of-life.solution-section')
@include('partials.tree-of-life.results-section')
@include('partials.tree-of-life.comment-section')
@include('partials.tree-of-life.form-section')
@endsection
