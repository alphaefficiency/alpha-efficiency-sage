{{--
  Template Name: Services - Development
--}}

@extends('layouts.app')

@section('content')

  <?php
    $catID = get_cat_ID('Web Design & Development');
  ?>

  @include('partials.services-development.header')

  @include('partials.services-development.get-started')

  @include('partials.services-development.first-part')

  {{-- @include('partials.blog.blog-recent')

  @include('partials.services-development.get-started')

  @include('partials.services-email.big-article')

  @include('partials.services-schedule') --}}

  @component ('components.services.featured-articles', ['title' => 'Featured Articles on', 'bold' => 'Development', 'catID' =>  $catID]) @endcomponent

@endsection

