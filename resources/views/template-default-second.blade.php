{{--
  Template Name: Second default page
--}}

@extends('layouts.app')

@section('content')

@include('partials.second-default.hero')
@include('components.default-page.content')
@endsection
