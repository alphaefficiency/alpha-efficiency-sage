{{--
  Template Name: Default page
--}}

@extends('layouts.app')

@section('content')

  @include('partials.default.default-section')
  @include('partials.blog.blog-recent')
@endsection