{{-- Single post article on Blog page (not Single Page Post) --}}
<?php
        $attachments = get_posts(array(
          'post_type' => 'attachment', 
          'post_mime_type'=>'image', 
          'posts_per_page' => 0, 
          'post_parent' => $post->ID, 
          'order'=>'ASC'
        ));
        $trending = get_post_meta($post->ID)['trending'] [0];
        var_dump($trending)
?>


<article class="single-post">
<div class="post-content-wrapper">

  <div class="post-title-wrapper">
    @if ($trending)
    <div class="dots-trending">
      <div class="dots">
        <img class="post-thumbnail-1" src="@asset('images/blog/four-dots.svg')" alt="Post image">
      </div>
      <div class="trending d-none">
        <img class="post-thumbnail" src="@asset('images/blog/trending.svg')" alt="Post image">
      </div>
    </div>
    @endif
    <a class="post-image-link" href="{{ get_permalink($post->ID) }}">
      @if (has_post_thumbnail($post->ID))
        <?=get_the_post_thumbnail($post->ID, 'full')?>
      @elseif (!$attachments)
        <img class="post-thumbnail" src="@asset('images/blog/post-image-holder-min.png')" alt="Post image">
      @else
        <img class="post-thumbnail" src="<?=$attachments[0]->guid?>" alt="Post image"/>
      @endif
      
    </a>
  </div>

  <div class="entry-content">
    <div class="time-title">
      <div class="time">
          <?= get_the_date('F j, Y', $post->ID)?>
        </div>
        <h2>
          <a class="post-title" href="{{ get_permalink($post->ID) }}">
            {{-- {{ Posts::getExcerptForPostTitle($post->post_title) }} --}}
          </a>
        </h2>
        <hr>
    </div>

    <div class="post-desc-div">
      <p class="post-description">
        @php echo Posts::getExcerptForPostContent($post->guid , $post->post_content) @endphp
      </p>
      {{-- Read More button --}}
      <a class="read-more-btn" href="{{ get_permalink($post->ID) }}">Read More</a>
    </div>
  </div>

</div>
</article>