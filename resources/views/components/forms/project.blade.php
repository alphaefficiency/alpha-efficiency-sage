<form action="#" id="project-form" autocomplete="off">
  <h5>{!! $title !!}</h5>
  <div class="form-body">
    <p>Yes, I'm interested in discussing a</p>
    {{-- <input type="text" placeholder="web design, dev, etc." id="project-type" required/> --}}
    <select class="minimal redText rotate" id="mySelect" required> 
      <option class="active" value="Development">Web Development Project</option>
      <option value="Email Marketing">Branding Project</option>
      <option value="PPC">SEO Campaigns Project</option>
      <option value="SEO">Google Ads Project</option>
      <option value="Content Writing">Facebook Ads Project</option>
      <option value="Website Design">Graphic Design Project</option>
    </select> 
    {{-- <select class="minimal redText" id="mySelect" required>
      <option class="active" value="Development">Web Development</option>
      <option value="Email Marketing">Branding</option>
      <option value="PPC">SEO Campaigns</option>
      <option value="SEO">Google Ads</option>
      <option value="Content Writing">Facebook Ads</option>
      <option value="Website Design">Graphic Design</option>
    </select> --}}
    <br class="br">
    <p>for</p>
    <input type="text" id="business-name" required placeholder="business name">
    <p class="name">. My name is</p>
    <input type="text" id="client-name" required placeholder="name"/><br class="mobile-break-f">
    <p>and you can email me </p>
    <br class="d-none">
    <input type="email" id="client-email-addr" required placeholder="example@example.com" />
  </div>
  <button type="submit" class="submit-btn">Submit</button>
</form>
