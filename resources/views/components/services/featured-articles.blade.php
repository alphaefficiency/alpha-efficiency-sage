<?php
  $featured_articles = get_posts(array(
    'numberposts'      => 3,
    'orderby'          => 'date',
    'order'            => 'DESC',
    'category'         => $catID,
  ));
?>

<section class="featured-articles-section">
  <div class="featured-title">
    <h2 class="title-title">{{$title}}</h2>
    <h2 class="title-bold">{{$bold}}</h2>
    <hr>
  </div>
  <div class="bottom-articles">
    @foreach($featured_articles as $article)
      <div class="article">
        <div class="image">
          @if (has_post_thumbnail($article->ID))
            <?=get_the_post_thumbnail($article->ID, 'full')?>
          @else
            <img class="post-thumbnail" src="@asset('images/blog/post-image-holder-min.png')" alt="Post image">
          @endif
        </div>
        <div class="text">
          <div class="date"><?= get_the_date('d. M Y', $article->ID)?></div>
          <div class="title"><h3>{{ $article->post_title }}</h3></div>
          <div class="text-content">
            {{ Posts::getExcerptForPostContent($article->link, $article->post_content, 100) }}
          </div>
          <a href="{{ get_post_permalink($article->ID) }}" class="read-more">Read More</a>
        </div>
      </div>
    @endforeach
  </div>
</section>
