<div class="portfolio-project-info">
  <div class="item">
    <h6>Project Duration</h6>
    <p>{{ $details['duration'] }} @php echo $details['duration'] == 1 ? 'month' : 'months' @endphp</p>
    <img src="@asset('images/portfolio/icons/duration.svg')" width="45" height="45" alt="Project Duration"/>
  </div>
  <div class="item">
    <h6>Estimated man-hours</h6>
    <p>{{ $details['hours'] }}</p>
    <img src="@asset('images/portfolio/icons/hours.svg')" width="45" height="45" alt="Man Hours"/>
  </div>
  <div class="item">
    <h6>Number of Team Members</h6>
    <p>{{ $details['members'] }}</p>
    <img src="@asset('images/portfolio/icons/members.svg')" width="45" height="45" alt="Number of Team Members"/>
  </div>
  <a href="{{ $details['link'] }}" class="detail-btn demo" target="_blank">View {{ $details['demo'] }}</a>
  {{-- <a href="#" class="detail-btn detail-up-btn">
    <svg width="29" height="19" viewBox="0 0 29 19" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M1 12L14.8 1L28 12" stroke="#fff"/>
      <path d="M1 18L14.8 7L28 18" stroke="#fff"/>
    </svg>      
  </a> --}}
</div>
