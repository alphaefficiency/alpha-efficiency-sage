{{--
  Template Name: About - Bane
--}}

@extends('layouts.app')

@section('content')

  @include('partials.about-single.us-bane.hero-section')

  @include('partials.about-single.us-bane.main-section')

@endsection
