{{--
  Template Name: Services - Social Media
--}}

@extends('layouts.app')

@section('content')

  <?php
    $catID = get_cat_ID('Social Media');
  ?>

  @include('partials.services-socialMedia.header')

  @include('partials.services-socialMedia.first-part')

  {{-- @include('partials.blog.blog-recent')

  @include('partials.services-SEM.get-started')

  @include('partials.services-socialMedia.big-article')

  @include('partials.services-schedule') --}}

  @component ('components.services.featured-articles', ['title' => 'Featured Articles on', 'bold' => 'Social Media', 'catID' =>  $catID]) @endcomponent

@endsection

