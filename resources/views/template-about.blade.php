{{--
  Template Name: About Careers
--}}

@extends('layouts.app')

@section('content')
  @include('partials.about.header')
  @include('partials.about.team')
  @include('partials.about.improved-productivity')
  @include('partials.about.clients')
  @include('partials.about.project')
@endsection