{{--
  Template Name: About - Teodora
--}}

@extends('layouts.app')

@section('content')

  @include('partials.about-single.us-teodora.hero-section')

  @include('partials.about-single.us-teodora.main-section')

@endsection

