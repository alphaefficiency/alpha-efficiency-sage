{{--
  Template Name: Case Study Sverdloff
--}}

@extends('layouts.app')

@section('content')

@include('partials.sverdloff.hero-study')
@include('partials.sverdloff.img-surge')
@include('partials.sverdloff.usability-section')
@include('partials.sverdloff.problem-section')
@include('partials.sverdloff.solution-section')
@include('partials.sverdloff.results-section')
@include('partials.sverdloff.comment-section')
@include('partials.sverdloff.form-section')
@endsection
