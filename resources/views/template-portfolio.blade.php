{{--
  Template Name: Portfolio
--}}

@extends('layouts.app')

@section('content')

  @include('partials.portfolio.hero')
  @include('partials.portfolio.asia-actual')
  @include('partials.portfolio.surge-strenght')
  @include('partials.portfolio.lineage')
  @include('components.portfolio.schedule')
  @include('partials.portfolio.mup')
  @include('partials.portfolio.breachlock')
  @include('partials.portfolio.better-weigh-medical')
  @include('components.portfolio.book')
  @include('partials.portfolio.aeriform')
  @include('partials.portfolio.paystubs')
  @include('partials.portfolio.tree-of-life')
  @include('components.portfolio.schedule')
  @include('partials.portfolio.surve')
  @include('partials.portfolio.coffee-with-us')
  @include('partials.portfolio.performance')
  @include('partials.portfolio.view-projects')
  @include('components.portfolio.book')
  @include('components.portfolio.schedule')

@endsection
