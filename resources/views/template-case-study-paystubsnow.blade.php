{{--
  Template Name: Case Study Paystubsnow
--}}

@extends('layouts.app')

@section('content')

@include('partials.paystubsnow.hero-study')
@include('partials.paystubsnow.img-surge')
@include('partials.paystubsnow.usability-section')
@include('partials.paystubsnow.problem-section')
@include('partials.paystubsnow.solution-section')
@include('partials.paystubsnow.results-section')
@include('partials.paystubsnow.comment-section')
@include('partials.paystubsnow.form-section')
@endsection
