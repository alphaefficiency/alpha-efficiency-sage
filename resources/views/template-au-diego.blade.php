{{--
  Template Name: About - Diego
--}}

@extends('layouts.app')

@section('content')

  @include('partials.about-single.us-diego.hero-section')

  @include('partials.about-single.us-diego.main-section')

@endsection
