{{--
  Template Name: Locations Template
--}}

@extends('layouts.app')

@section('content')
@include('partials.locations.hero')
@include('partials.locations.third-section')
@include('partials.locations.fourth-section')
@include('partials.locations.fifth-section')
@endsection