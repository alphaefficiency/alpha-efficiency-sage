{{--
  Template Name: About - Ethan
--}}

@extends('layouts.app')

@section('content')

  @include('partials.about-single.us-ethan.hero-section')

  @include('partials.about-single.us-ethan.main-section')

@endsection
