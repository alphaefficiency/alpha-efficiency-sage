{{--
  Template Name: Case Study Multi Unit
--}}

@extends('layouts.app')

@section('content')

@include('partials.multi-unit.hero-study')
@include('partials.multi-unit.img-surge')
@include('partials.multi-unit.usability-section')
@include('partials.multi-unit.problem-section')
@include('partials.multi-unit.solution-section')
@include('partials.multi-unit.results-section')
@include('partials.multi-unit.comment-section')
@include('partials.multi-unit.form-section')
@endsection
