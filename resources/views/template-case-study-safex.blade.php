{{--
  Template Name: Case Study Safex
--}}

@extends('layouts.app')

@section('content')

@include('partials.safex.hero-study')
@include('partials.safex.img-surge')
@include('partials.safex.usability-section')
@include('partials.safex.problem-section')
@include('partials.safex.solution-section')
@include('partials.safex.results-section')
@include('partials.safex.comment-section')
@include('partials.safex.form-section')
@endsection
