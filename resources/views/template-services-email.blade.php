{{--
  Template Name: Services - Email
--}}

@extends('layouts.app')

@section('content')

  <?php
    $catID = get_cat_ID('Email Marketing');
  ?>

  @include('partials.services-email.header')

  @include('partials.services-email.get-started')

  @include('partials.services-email.first-part')

  {{-- @include('partials.blog.blog-recent')

  @include('partials.services-email.get-started')

  @include('partials.services-email.big-article')

  @include('partials.services-schedule') --}}

  @component ('components.services.featured-articles', ['title' => 'Featured Articles on', 'bold' => 'Email Solution', 'catID' =>  $catID]) @endcomponent

@endsection

