{{--
  Template Name: About - Brian
--}}

@extends('layouts.app')

@section('content')

  @include('partials.about-single.us-brian.hero-section')

  @include('partials.about-single.us-brian.main-section')

@endsection

