{{--
  Template Name: Privacy Policy
--}}

@extends('layouts.app')

@section('content')

  @include('partials.privacy.hero-content')

  @include('partials.privacy.wrepper-content')

@endsection
