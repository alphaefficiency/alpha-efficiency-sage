{{--
  Template Name: About - Sara
--}}

@extends('layouts.app')

@section('content')

  @include('partials.about-single.au-sara.hero-content')

  @include('partials.about-single.au-sara.main-content')

@endsection