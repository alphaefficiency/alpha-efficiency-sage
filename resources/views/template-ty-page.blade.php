{{--
  Template Name: TY page
--}}

@extends('layouts.app')

    @section('content')
    <div class="holder">
        <div class="container">
        <div class="ty-section">
            <h2>Thank You For Submitting!</h2>
            <p>Continue Searching the Website</p>
            <a href="{{get_site_url()}}">Back to Home</a>
        </div>
        <div class="wrapper">
			<h1>Make Digital Profitable.</h1>
			<h1><b>Book Your Appointment</b></h1>
			<hr>
			<small>We’ll establish your game plan & prepare you for success.</small>
		</div>
        <!-- Start of Meetings Embed Script -->
        <div class="meetings-iframe-container" data-src="https://meetings.hubspot.com/bojan?embed=true"></div>
        <script type="text/javascript" src="https://static.hsappstatic.net/MeetingsEmbed/ex/MeetingsEmbedCode.js"></script>
        <!-- End of Meetings Embed Script -->
        </div>

        <div class="bg-desktop">
        <svg width="1920" height="535" viewBox="0 0 1920 535" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M1924.55 309.551C1924.55 309.551 1512.43 317.123 1156.05 353.012C799.673 388.901 768.547 297.807 413.654 333.547C57.2754 369.436 -6.99859 282.402 -6.99859 282.402" stroke="#FFE500" stroke-width="2"/>
            <ellipse rx="6.41587" ry="6.49301" transform="matrix(0.999672 0.0256304 -0.0210393 0.999779 1354.45 335.656)" fill="#FFEC00"/>
            <ellipse rx="6.41587" ry="6.49301" transform="matrix(0.999672 0.0256304 -0.0210393 0.999779 1354.45 380.333)" fill="#1994FF"/>
            <ellipse rx="6.41587" ry="6.49301" transform="matrix(0.999672 0.0256304 -0.0210393 0.999779 1354.45 355.656)" fill="#FFB000"/>
            <ellipse rx="6.41587" ry="6.49301" transform="matrix(0.999672 0.0256304 -0.0210393 0.999779 1354.45 308.656)" fill="#ED462F"/>
            <path d="M1921.53 292.582C1921.53 292.582 1532.54 363.444 1176.16 399.332C819.779 435.221 763.04 281.684 408.146 317.423C51.7679 353.312 -6.12935 269.751 -6.12935 269.751" stroke="#1994FF" stroke-width="2"/>
            <path d="M1922.14 278.39C1922.14 278.39 1528.95 340.304 1172.58 376.193C816.198 412.082 759.456 271.205 404.562 306.944C48.1838 342.833 -28.0001 186.808 -28.0001 186.808" stroke="#FFB000" stroke-width="2"/>
            <path d="M1923.34 350.585C1923.34 350.585 1501.41 284.878 1145.03 320.767C788.654 356.656 753.947 255.082 399.054 290.821C42.6753 326.71 -4.99911 250.835 -4.99911 250.835" stroke="#ED462F" stroke-width="2"/>
        </svg>
        </div>

        <div class="for-phone">
        <svg width="375" height="318" viewBox="0 0 375 318" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M-132.365 183.848C-132.365 183.848 13.4181 188.345 139.483 209.66C265.547 230.975 276.558 176.873 402.097 198.099C528.161 219.414 550.897 167.723 550.897 167.723" stroke="#FFE500" stroke-width="2"/>
            <ellipse rx="2.13784" ry="2.16354" transform="matrix(0.982803 0.184658 -0.180143 0.98364 143.177 210.14)" fill="#FFEC00"/>
            <path d="M-131.297 173.77C-131.297 173.77 6.30609 215.855 132.371 237.171C258.435 258.486 278.506 167.297 404.045 188.523C530.11 209.838 550.59 160.21 550.59 160.21" stroke="#1994FF" stroke-width="2"/>
            <ellipse rx="2.13784" ry="2.16354" transform="matrix(0.982803 0.184658 -0.180143 0.98364 143.177 238.646)" fill="#1994FF"/>
            <path d="M-131.51 165.341C-131.51 165.341 7.57296 202.113 133.637 223.428C259.702 244.743 279.774 161.073 405.313 182.3C531.377 203.615 558.327 110.949 558.327 110.949" stroke="#FFB000" stroke-width="2"/>
            <ellipse rx="2.13784" ry="2.16354" transform="matrix(0.982803 0.184658 -0.180143 0.98364 143.177 224.8)" fill="#FFB000"/>
            <path d="M-131.936 208.219C-131.936 208.219 17.3163 169.194 143.381 190.509C269.445 211.824 281.722 151.498 407.261 172.724C533.326 194.039 550.19 148.976 550.19 148.976" stroke="#ED462F" stroke-width="2"/>
            <ellipse rx="2.13784" ry="2.16354" transform="matrix(0.982803 0.184658 -0.180143 0.98364 143.381 190.185)" fill="#ED462F"/>
        </svg>        
        </div>
    </div>

    <div class="form">
        <div class="form-text">
        <div class="text">
            <p>Fresh inspiration is a fingertip away, <b>Download Our Portfolio.</b></p>
            <hr>
        </div>
        <div class="form-holder">
            <div class="text-upper">
            <p>Fill out the form.</p>
            <hr>
            </div>

            <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/shell.js"></script>
    <script>
    hbspt.forms.create({
        portalId: "4773320",
        formId: "bf3114ed-d1e8-44d2-877e-0a92a7ed4b9a"
    });
    </script>
        </div>
        </div>
        <div class="bg-desktop">
        <svg width="1920" height="535" viewBox="0 0 1920 535" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M-6.55139 309.551C-6.55139 309.551 405.571 317.123 761.949 353.012C1118.33 388.901 1149.45 297.807 1504.35 333.547C1860.73 369.436 1925 282.402 1925 282.402" stroke="#FFE500" stroke-width="2"/>
            <ellipse rx="6.41587" ry="6.49301" transform="matrix(-0.999672 0.0256304 0.0210393 0.999779 563.55 335.656)" fill="#FFEC00"/>
            <ellipse rx="6.41587" ry="6.49301" transform="matrix(-0.999672 0.0256304 0.0210393 0.999779 563.55 380.333)" fill="#1994FF"/>
            <ellipse rx="6.41587" ry="6.49301" transform="matrix(-0.999672 0.0256304 0.0210393 0.999779 563.55 355.656)" fill="#FFB000"/>
            <ellipse rx="6.41587" ry="6.49301" transform="matrix(-0.999672 0.0256304 0.0210393 0.999779 563.55 308.656)" fill="#ED462F"/>
            <path d="M-3.53285 292.582C-3.53285 292.582 385.465 363.444 741.844 399.332C1098.22 435.221 1154.96 281.684 1509.85 317.423C1866.23 353.312 1924.13 269.751 1924.13 269.751" stroke="#1994FF" stroke-width="2"/>
            <path d="M-4.13493 278.39C-4.13493 278.39 389.046 340.304 745.425 376.193C1101.8 412.082 1158.55 271.205 1513.44 306.944C1869.82 342.833 1946 186.808 1946 186.808" stroke="#FFB000" stroke-width="2"/>
            <path d="M-5.34042 350.585C-5.34042 350.585 416.59 284.878 772.969 320.767C1129.35 356.656 1164.05 255.082 1518.95 290.821C1875.33 326.71 1923 250.835 1923 250.835" stroke="#ED462F" stroke-width="2"/>
        </svg>
        </div>
        <div class="for-phone">
        <svg width="375" height="318" viewBox="0 0 375 318" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M564.691 183.848C564.691 183.848 418.908 188.345 292.844 209.66C166.779 230.975 155.769 176.873 30.2294 198.099C-95.835 219.414 -118.571 167.723 -118.571 167.723" stroke="#FFE500" stroke-width="2"/>
            <ellipse rx="2.13784" ry="2.16354" transform="matrix(-0.982803 0.184658 0.180143 0.98364 289.149 210.14)" fill="#FFEC00"/>
            <ellipse rx="2.13784" ry="2.16354" transform="matrix(-0.982803 0.184658 0.180143 0.98364 37.4576 196.701)" fill="#FFEC00"/>
            <path d="M563.623 173.77C563.623 173.77 426.02 215.855 299.956 237.171C173.891 258.486 153.82 167.297 28.2812 188.523C-97.7833 209.838 -118.264 160.21 -118.264 160.21" stroke="#1994FF" stroke-width="2"/>
            <ellipse rx="2.13784" ry="2.16354" transform="matrix(-0.982803 0.184658 0.180143 0.98364 289.149 238.646)" fill="#1994FF"/>
            <ellipse rx="2.13784" ry="2.16354" transform="matrix(-0.982803 0.184658 0.180143 0.98364 37.4576 187.334)" fill="#1994FF"/>
            <path d="M563.836 165.341C563.836 165.341 424.753 202.113 298.689 223.428C172.624 244.743 152.552 161.073 27.0132 182.3C-99.0513 203.615 -126 110.949 -126 110.949" stroke="#FFB000" stroke-width="2"/>
            <ellipse rx="2.13784" ry="2.16354" transform="matrix(-0.982803 0.184658 0.180143 0.98364 289.149 224.8)" fill="#FFB000"/>
            <ellipse rx="2.13784" ry="2.16354" transform="matrix(-0.982803 0.184658 0.180143 0.98364 37.4576 180.818)" fill="#FFB000"/>
            <path d="M564.263 208.219C564.263 208.219 415.01 169.194 288.945 190.509C162.881 211.824 150.604 151.498 25.0647 172.724C-101 194.039 -117.864 148.976 -117.864 148.976" stroke="#ED462F" stroke-width="2"/>
            <ellipse rx="2.13784" ry="2.16354" transform="matrix(-0.982803 0.184658 0.180143 0.98364 288.945 190.185)" fill="#ED462F"/>
            <ellipse rx="2.13784" ry="2.16354" transform="matrix(-0.982803 0.184658 0.180143 0.98364 37.4576 171.044)" fill="#ED462F"/>
        </svg>        
        </div>
    </div>
    @endsection
