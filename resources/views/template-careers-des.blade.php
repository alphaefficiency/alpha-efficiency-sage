{{--
  Template Name: Careers Designer
--}}

@extends('layouts.app')

@section('content')

  @include('partials.careers-des.hero')

  @include('partials.careers-des.wrepper')
  
  @include('partials.careers.career-single-grow')

  @include('partials.careers.career-single-form')

@endsection