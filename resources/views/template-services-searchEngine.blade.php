{{--
  Template Name: Services - Search Engine Marketing
--}}

@extends('layouts.app')

@section('content')

  <?php
    $catID = get_cat_ID('Digital Marketing');
  ?>

  @include('partials.services-SEM.header')

  @include('partials.services-SEM.get-started')

  @include('partials.services-SEM.first-part')

  {{-- @include('partials.blog.blog-recent')

  @include('partials.services-SEM.get-started')

  @include('partials.services-SEM.big-article')

  @include('partials.services-schedule') --}}

  @component ('components.services.featured-articles', ['title' => 'Featured Articles on', 'bold' => 'Search Engine Marketing', 'catID' =>  $catID]) @endcomponent

@endsection

