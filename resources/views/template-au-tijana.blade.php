{{--
  Template Name: About - Tijana
--}}

@extends('layouts.app')

@section('content')

  @include('partials.about-single.us-tijana.hero-section')

  @include('partials.about-single.us-tijana.main-section')

@endsection

