{{--
  Template Name: Case Study Coffee With Us
--}}

@extends('layouts.app')

@section('content')

@include('partials.coffee-with-us.hero-study')
@include('partials.coffee-with-us.img-surge')
@include('partials.coffee-with-us.usability-section')
@include('partials.coffee-with-us.problem-section')
@include('partials.coffee-with-us.solution-section')
@include('partials.coffee-with-us.results-section')
@include('partials.coffee-with-us.comment-section')
@include('partials.coffee-with-us.form-section')
@endsection
