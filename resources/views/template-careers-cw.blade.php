{{--
  Template Name: Careers content writer
--}}

@extends('layouts.app')

@section('content')

  @include('partials.careers-cw.hero')

  @include('partials.careers-cw.wrepper')
  
  @include('partials.careers.career-single-grow')

  @include('partials.careers.career-single-form')

@endsection