<?php

namespace App\Controllers;

use Sober\Controller\Controller;
use WP_Query;

class Posts extends Controller
{
  static public function getAllPosts()
  {
    $catquery = new WP_Query(array(
      // 'category_name' => 'Immigration News',
      'post_type'        => 'post'
      // 'post_type'      => 'post',
      // Which page to display
      // 'paged'          => 1
    ));

    $posts = $catquery->posts;
    foreach ($posts as $post) {
      $post->photo = get_the_post_thumbnail_url($post->ID); // == null ? "@asset('images/home/before-form.png')" : get_the_post_thumbnail_url($post->ID);
      $post->link = get_permalink($post->ID);
    }
    return $posts;
  }

  static public function getPostsByPage($postsPerPage, $page)
  {
    $catquery = new WP_Query(array(
      // 'category_name' => 'Immigration News',
      'post_type'      => 'post',
      'posts_per_page' => $postsPerPage,
      // Which page to display
      'paged'          => $page
    ));

    $posts = $catquery->posts;
    foreach ($posts as $post) {
      $post->photo = get_the_post_thumbnail_url($post->ID); // == null ? "@asset('images/home/before-form.png')" : get_the_post_thumbnail_url($post->ID);
      $post->link = get_permalink($post->ID);
    }
    return $posts;
  }

  static public function pagination($postsPerPage)
  {
    // Get only published posts
    $totalPostsCounted = wp_count_posts()->publish;
    return intval(ceil($totalPostsCounted / $postsPerPage));
  }

  // static public function getExcerptForPostTitle($title)
  // {
  //   $excerpt = $title;
  //   // $excerpt = preg_replace(" ([*])",'',$excerpt);
  //   $excerpt = strip_shortcodes($excerpt);
  //   $excerpt = strip_tags($excerpt);
  //   $exploded = explode(" ", $excerpt);
  //   $str = "";
  //   for ($i = 0; $i <= count($exploded); $i++) {
  //     if (strlen($str) < 200) {
  //       $str = $str . " " . $exploded[$i];
  //     } else {
  //       $str = $str . '...';
  //       break;
  //     }
  //   }

  //   return $str;
  // }

  static public function getExcerptForBlogCarouselTitle($title)
  {
    $excerpt = $title;
    // $excerpt = preg_replace(" ([*])",'',$excerpt);
    $excerpt = strip_shortcodes($excerpt);
    $excerpt = strip_tags($excerpt);
    $exploded = explode(" ", $excerpt);
    $str = "";
    for ($i = 0; $i <= count($exploded); $i++) {
      if (strlen($str) < 60) {
        $str = $str . " " . $exploded[$i];
      } else {
        $str = $str . '...';
        break;
      }
    }

    return $str;
  }

  static public function getExcerptForPostContent($link, $content, $amount = 150)
  {
    $excerpt = $content;
    // $excerpt = preg_replace(" ([*])",'',$excerpt);
    $excerpt = strip_shortcodes($excerpt);
    $excerpt = strip_tags($excerpt);
    $str = "";
    for ($i = 0; $i <= strlen($excerpt); $i++) {
      if (strlen($str) < $amount) {
        $str = $str . " " . substr($excerpt, 0, $amount);
      } else {
        $str = $str . '...';
        break;
      }
    }

    return $str;
  }

  static public function getCurrentPage($pageNumber, $totalPages)
  {
    $is404 = null;
    $currentPage = null;

    if (is_null($pageNumber)) {
      $is404 = false;
      $currentPage = 1;
    } else {
      $isNumber = intval($pageNumber);
      if ($isNumber !== null && ($pageNumber > $totalPages || $pageNumber < 1)) {
        $is404 = true;
        // Set current page to 1 if its the /blog (root) page
      } else {
        $is404 = false;
        $currentPage = $pageNumber;
      }
    }

    $response = (object) [
      'currentPage' => intval($currentPage),
      'is404' => $is404
    ];
    return $response;
  }

  static public function getPostsByCategoryId($postsPerPage, $currentPage, $categoryId) {
    $catquery = new WP_Query(array(
        //'category_name' => 'Immigration News',
        'post_type'      => 'post',
        'posts_per_page' => $postsPerPage,
        // Which page to display
        'paged'          => $currentPage,
        'cat' => $categoryId
    )); 

    $posts = $catquery->posts;
    foreach ($posts as $post) {
      $post->photo = get_the_post_thumbnail_url($post->ID) == null ? "no-img" : get_the_post_thumbnail_url($post->ID);
      $post->link = get_permalink($post->ID);
    } 
    return $posts;
  }

  // Pagination for Post categories
  static public function categoriesPagination($postCounted, $postsPerPage)
  {
    // Get only published posts
    return intval(ceil($postCounted / $postsPerPage));
  }

  static public function getPostsByTag($postsPerPage, $currentPage, $tagId) {
    $catquery = new WP_Query(array(
      'post_type'      => 'post',
      'posts_per_page' => $postsPerPage,
      // Which page to display
      'paged'          => $currentPage,
      'tag_id' => $tagId
    ));

    $posts = $catquery->posts;
    return $posts;
  }
}
