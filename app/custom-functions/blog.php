<?php

function getBlogPosts($num = null)
{
  if ($num == null) {
    return get_posts();
  } else {

    $args = array(
      "posts_per_page" => $num,
      "orderby"        => "date",
    );

    return wp_get_recent_posts($args);
  }
}
